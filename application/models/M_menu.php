<?php 

// WWW.MALASNGODING.COM === Author : Diki Alfarabi Hadi
// Model yang terstruktur. agar bisa digunakan berulang kali untuk membuat CRUD. 
// Sehingga proses pembuatan CRUD menjadi lebih cepat dan efisien.

class M_menu extends CI_Model {
	
    function json_where($field,$table,$id,$controller,$where) {
        $this->datatables->select($field);
        $this->datatables->from($table);
        $this->datatables->where($where);
        $this->datatables->add_column('action_up_down',
            anchor(site_url($controller.'/up/$1'),'<i class="fa fa-arrow-up" aria-hidden="true" title="Up"></i>', array('class' => 'btn btn-info btn-sm')).' '.
            anchor(site_url($controller.'/down/$1'),'<i class="fa fa-arrow-down" aria-hidden="true" title="Down"></i>', array('class' => 'btn btn-instagram btn-sm')), $id);
        $this->datatables->add_column('action_edit',
            anchor(site_url($controller.'/edit/$1'),'<i class="fa fa-pencil-square-o" aria-hidden="true" title="Edit Data"></i>', array('class' => 'btn btn-facebook btn-sm')), $id);
        $this->datatables->add_column('action_detail',
            anchor(site_url($controller.'/detail/$1'),'<i class="fa fa-search" aria-hidden="true" title="Detail Data"></i>', array('class' => 'btn btn-foursquare btn-sm')), $id);
        $this->datatables->add_column('action_delete',
            anchor(site_url($controller.'/hapus/$1'),'<i class="fa fa-trash-o" aria-hidden="true" title="Hapus Data"></i>','class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Anda Yakin Mau di Hapus ?\')"'), $id);        
//            $this->datatables->add_column('action',
//                    anchor(site_url($controller.'/edit/$1'),'<i class="fa fa-pencil-square-o" aria-hidden="true" title="Edit Data"></i>', array('class' => 'btn btn-facebook btn-sm'))." ".
//                    anchor(site_url($controller.'/detail/$1'),'<i class="fa fa-search" aria-hidden="true" title="Detail Data"></i>', array('class' => 'btn btn-foursquare btn-sm'))." ".
//                    anchor(site_url($controller.'/hapus/$1'),'<i class="fa fa-trash-o" aria-hidden="true" title="Hapus Data"></i>','class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Anda Yakin Mau di Hapus ?\')"'), $id);
        return $this->datatables->generate();
    }
    
}

?>