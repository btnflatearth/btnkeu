<?php 

// WWW.MALASNGODING.COM === Author : Diki Alfarabi Hadi
// Model yang terstruktur. agar bisa digunakan berulang kali untuk membuat CRUD. 
// Sehingga proses pembuatan CRUD menjadi lebih cepat dan efisien.

class M_data extends CI_Model{
	
	function cek_login($table,$where){
		return $this->db->get_where($table,$where);
	}
        
        function json($field,$table,$id,$controller) {
            $this->datatables->select($field);
            $this->datatables->from($table);
            $this->datatables->add_column('action_edit',
                anchor(site_url($controller.'/edit/$1'),'<i class="fa fa-pencil-square-o" aria-hidden="true" title="Edit Data"></i>', array('class' => 'btn btn-facebook btn-sm')), $id);
            $this->datatables->add_column('action_detail',
                anchor(site_url($controller.'/detail/$1'),'<i class="fa fa-search" aria-hidden="true" title="Detail Data"></i>', array('class' => 'btn btn-dropbox btn-sm')), $id);
            $this->datatables->add_column('action_delete',
                anchor(site_url($controller.'/hapus/$1'),'<i class="fa fa-trash-o" aria-hidden="true" title="Hapus Data"></i>','class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Anda Yakin Mau di Hapus ?\')"'), $id);  
            $this->datatables->add_column('action_soft_delete',
                anchor(site_url($controller.'/soft_hapus/$1'),'<i class="fa fa-trash-o" aria-hidden="true" title="Hapus Data"></i>','class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Anda Yakin Mau di Hapus ?\')"'), $id);  
            $this->datatables->add_column('action_cetak',
                anchor(site_url($controller.'/cetak/$1'),'<i class="fa fa-print" aria-hidden="true" title="Cetak Data"></i>', array('class' => 'btn btn-dropbox btn-sm')), $id);
//            $this->datatables->add_column('action',
//                    anchor(site_url($controller.'/edit/$1'),'<i class="fa fa-pencil-square-o" aria-hidden="true" title="Edit Data"></i>', array('class' => 'btn btn-facebook btn-sm'))." ".
//                    anchor(site_url($controller.'/detail/$1'),'<i class="fa fa-search" aria-hidden="true" title="Detail Data"></i>', array('class' => 'btn btn-foursquare btn-sm'))." ".
//                    anchor(site_url($controller.'/hapus/$1'),'<i class="fa fa-trash-o" aria-hidden="true" title="Hapus Data"></i>','class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Anda Yakin Mau di Hapus ?\')"'), $id);
            return $this->datatables->generate();
        }
        
        function json_menu($field,$table,$id,$controller,$string_menu) {
            $this->datatables->select($field);
            $this->datatables->from($table);
            
            $this->datatables->add_column('action_edit',
                anchor(site_url($controller.'/edit/$1'),'<i class="fa fa-pencil-square-o" aria-hidden="true" title="Edit Data"></i>', array('class' => 'btn btn-facebook btn-sm')), $id);
            $this->datatables->add_column('action_detail',
                anchor(site_url($controller.'/detail/$1/'.$string_menu),'<i class="fa fa-search" aria-hidden="true" title="Detail Data"></i>', array('class' => 'btn btn-dropbox btn-sm')), $id);
            $this->datatables->add_column('action_delete',
                anchor(site_url($controller.'/hapus/$1'),'<i class="fa fa-trash-o" aria-hidden="true" title="Hapus Data"></i>','class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Anda Yakin Mau di Hapus ?\')"'), $id);  
            return $this->datatables->generate();
        }
        
        function json_menu_report($field,$table,$id,$controller,$string_menu) {
            $this->datatables->select($field);
            $this->datatables->from($table);
            
            $this->datatables->add_column('action_cetak',
                anchor(site_url($controller.'/cetak/$1'),'<i class="fa fa-print" aria-hidden="true" title="Cetak"></i>', array('class' => 'btn btn-facebook btn-sm')), $id);
            return $this->datatables->generate();
        }
        
        function json_user_unit_child($field,$table,$id,$controller,$id_value,$id_parent) {
            $this->datatables->select($field);
            $this->datatables->from($table);
            $this->datatables->where($id,$id_value);
            $this->datatables->add_column('action_edit',
                anchor(site_url($controller.'/edit_user_unit/$1'),'<i class="fa fa-pencil-square-o" aria-hidden="true" title="Edit Data"></i>', array('class' => 'btn btn-facebook btn-sm')), $id_parent);
            $this->datatables->add_column('action_detail',
                anchor(site_url($controller.'/detail_user_unit/$1'),'<i class="fa fa-search" aria-hidden="true" title="Detail Data"></i>', array('class' => 'btn btn-dropbox btn-sm')), $id_parent);
            $this->datatables->add_column('action_delete',
                anchor(site_url($controller.'/hapus_unit_user/$1'),'<i class="fa fa-trash-o" aria-hidden="true" title="Hapus Data"></i>','class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Anda Yakin Mau di Hapus ?\')"'), $id_parent);        
//            $this->datatables->add_column('action',
//                    anchor(site_url($controller.'/edit/$1'),'<i class="fa fa-pencil-square-o" aria-hidden="true" title="Edit Data"></i>', array('class' => 'btn btn-facebook btn-sm'))." ".
//                    anchor(site_url($controller.'/detail/$1'),'<i class="fa fa-search" aria-hidden="true" title="Detail Data"></i>', array('class' => 'btn btn-foursquare btn-sm'))." ".
//                    anchor(site_url($controller.'/hapus/$1'),'<i class="fa fa-trash-o" aria-hidden="true" title="Hapus Data"></i>','class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Anda Yakin Mau di Hapus ?\')"'), $id);
            return $this->datatables->generate();
        }
        
        function json_where($field,$table,$id,$controller,$where) {
            $this->datatables->select($field);
            $this->datatables->from($table);
            $this->datatables->where($where);
            $this->datatables->add_column('action_edit',
                anchor(site_url($controller.'/edit/$1'),'<i class="fa fa-pencil-square-o" aria-hidden="true" title="Edit Data"></i>', array('class' => 'btn btn-facebook btn-sm')), $id);
            $this->datatables->add_column('action_detail',
                anchor(site_url($controller.'/detail/$1'),'<i class="fa fa-search" aria-hidden="true" title="Detail Data"></i>', array('class' => 'btn btn-google btn-sm')), $id);
            $this->datatables->add_column('action_delete',
                anchor(site_url($controller.'/hapus/$1'),'<i class="fa fa-trash-o" aria-hidden="true" title="Hapus Data"></i>','class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Anda Yakin Mau di Hapus ?\')"'), $id);        
//            $this->datatables->add_column('action',
//                    anchor(site_url($controller.'/edit/$1'),'<i class="fa fa-pencil-square-o" aria-hidden="true" title="Edit Data"></i>', array('class' => 'btn btn-facebook btn-sm'))." ".
//                    anchor(site_url($controller.'/detail/$1'),'<i class="fa fa-search" aria-hidden="true" title="Detail Data"></i>', array('class' => 'btn btn-foursquare btn-sm'))." ".
//                    anchor(site_url($controller.'/hapus/$1'),'<i class="fa fa-trash-o" aria-hidden="true" title="Hapus Data"></i>','class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Anda Yakin Mau di Hapus ?\')"'), $id);
            return $this->datatables->generate();
        }
        
        function json_where_var($field,$table,$id,$controller,$where,$var) {
            $this->datatables->select($field);
            $this->datatables->from($table);
            $this->datatables->where($where);
            $this->datatables->add_column('action_edit',
                anchor(site_url($controller.'/edit'.$var.'/$1'),'<i class="fa fa-pencil-square-o" aria-hidden="true" title="Edit Data"></i>', array('class' => 'btn btn-facebook btn-sm')), $id);
            $this->datatables->add_column('action_detail',
                anchor(site_url($controller.'/detail/$1'),'<i class="fa fa-search" aria-hidden="true" title="Detail Data"></i>', array('class' => 'btn btn-google btn-sm')), $id);
            $this->datatables->add_column('action_delete',
                anchor(site_url($controller.'/hapus'.$var.'/$1'),'<i class="fa fa-trash-o" aria-hidden="true" title="Hapus Data"></i>','class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Anda Yakin Mau di Hapus ?\')"'), $id);        
//            $this->datatables->add_column('action',
//                    anchor(site_url($controller.'/edit/$1'),'<i class="fa fa-pencil-square-o" aria-hidden="true" title="Edit Data"></i>', array('class' => 'btn btn-facebook btn-sm'))." ".
//                    anchor(site_url($controller.'/detail/$1'),'<i class="fa fa-search" aria-hidden="true" title="Detail Data"></i>', array('class' => 'btn btn-foursquare btn-sm'))." ".
//                    anchor(site_url($controller.'/hapus/$1'),'<i class="fa fa-trash-o" aria-hidden="true" title="Hapus Data"></i>','class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Anda Yakin Mau di Hapus ?\')"'), $id);
            return $this->datatables->generate();
        }
        
        function json_where_menu($field,$table,$id,$controller,$where,$string_menu) {
            $this->datatables->select($field);
            $this->datatables->from($table);
            $this->datatables->where($where);
            $this->datatables->add_column('action_edit',
                anchor(site_url($controller.'/edit/$1'),'<i class="fa fa-pencil-square-o" aria-hidden="true" title="Edit Data"></i>', array('class' => 'btn btn-facebook btn-sm')), $id);
            $this->datatables->add_column('action_detail',
                anchor(site_url($controller.'/detail/$1/'.$string_menu),'<i class="fa fa-search" aria-hidden="true" title="Detail Data"></i>', array('class' => 'btn btn-foursquare btn-sm')), $id);
            $this->datatables->add_column('action_delete',
                anchor(site_url($controller.'/hapus/$1'),'<i class="fa fa-trash-o" aria-hidden="true" title="Hapus Data"></i>','class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Anda Yakin Mau di Hapus ?\')"'), $id);        
//            $this->datatables->add_column('action',
//                    anchor(site_url($controller.'/edit/$1'),'<i class="fa fa-pencil-square-o" aria-hidden="true" title="Edit Data"></i>', array('class' => 'btn btn-facebook btn-sm'))." ".
//                    anchor(site_url($controller.'/detail/$1'),'<i class="fa fa-search" aria-hidden="true" title="Detail Data"></i>', array('class' => 'btn btn-foursquare btn-sm'))." ".
//                    anchor(site_url($controller.'/hapus/$1'),'<i class="fa fa-trash-o" aria-hidden="true" title="Hapus Data"></i>','class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Anda Yakin Mau di Hapus ?\')"'), $id);
            return $this->datatables->generate();
        }
        
        function json_where_menu_var($field,$table,$id,$controller,$where,$string_menu,$var) {
            $this->datatables->select($field);
            $this->datatables->from($table);
            $this->datatables->where($where);
            $this->datatables->add_column('action_edit',
                anchor(site_url($controller.'/edit/$1/'.$string_menu.'/'.$var),'<i class="fa fa-pencil-square-o" aria-hidden="true" title="Edit Data"></i>', array('class' => 'btn btn-facebook btn-sm')), $id);
            $this->datatables->add_column('action_detail',
                anchor(site_url($controller.'/detail/$1'),'<i class="fa fa-search" aria-hidden="true" title="Detail Data"></i>', array('class' => 'btn btn-foursquare btn-sm')), $id);
            $this->datatables->add_column('action_delete',
                anchor(site_url($controller.'/hapus/$1/'.$string_menu.'/'.$var),'<i class="fa fa-trash-o" aria-hidden="true" title="Hapus Data"></i>','class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Anda Yakin Mau di Hapus ?\')"'), $id);        
//            $this->datatables->add_column('action',
//                    anchor(site_url($controller.'/edit/$1'),'<i class="fa fa-pencil-square-o" aria-hidden="true" title="Edit Data"></i>', array('class' => 'btn btn-facebook btn-sm'))." ".
//                    anchor(site_url($controller.'/detail/$1'),'<i class="fa fa-search" aria-hidden="true" title="Detail Data"></i>', array('class' => 'btn btn-foursquare btn-sm'))." ".
//                    anchor(site_url($controller.'/hapus/$1'),'<i class="fa fa-trash-o" aria-hidden="true" title="Hapus Data"></i>','class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Anda Yakin Mau di Hapus ?\')"'), $id);
            return $this->datatables->generate();
        }
        
	// FUNGSI CRUD
	// fungsi untuk mengambil data dari database
	function get_data($table){
		return $this->db->get($table);
	}
        
        function get_data_filter($table,$where){
		return $this->db->get_where($table,$where);
	}
        
        function get_data_filter_menu($table,$where,$field,$order){
                $this->db->order_by($field,$order);
		return $this->db->get_where($table,$where);
	}
                
        function get_data_filter_limit($table,$where,$field,$order,$limit){
                $this->db->limit($limit);
                $this->db->order_by($field,$order);
		return $this->db->get_where($table,$where);
	}

	// fungsi untuk menginput data ke database
	function insert_data($data,$table){
		$this->db->insert($table,$data);
	}

	// fungsi untuk mengedit data
	function edit_data($where,$table){
		return $this->db->get_where($table,$where);
	}

	// fungsi untuk mengupdate atau mengubah data di database
	function update_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}

	// fungsi untuk menghapus data dari database
	function delete_data($where,$table){
		$this->db->delete($table,$where);
	}
	// AKHIR FUNGSI CRUD
        
        // Ambil Jumlah Data
        function jumlah_data($table){
            return $this->db->get($table)->num_rows();
        }
        
        // Ambil Jumlah Data
        function jumlah_data_filter_limit($table,$where,$field,$order,$limit){
            $this->db->limit($limit);
            $this->db->order_by($field,$order);
            $this->db->where($where);
            return $this->db->get($table)->num_rows();
        }
        
        // Ambil Jumlah Data
        function insert_log($user_log_desc,$id){
            $q_user = $this->db->query("select * from aauth_users where id = $id")->row_array();
            $user_log = array(
                'log_user' => $user_log_desc,
                'name' => $q_user['name'],
                'id' => $id
            );
            return $this->db->insert('t_log_user',$user_log);
        }
        
        // Ambil Jumlah Data
        function insert_default_role($idr_group,$idt_user){
            $user_group = array(
                'idr_group' => $idr_group,
                'idt_user' => $idt_user
            );
            return $this->db->insert('t_user_to_group',$user_group);
        }
        
        // get data by id
        function get_by_id($table,$column_id,$id)
        {
            $this->db->where($column_id, $id);
            return $this->db->get($table)->row();
        }
	
}

?>