<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of C_menu
 *
 * @author Dicky Satria Utama
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class C_menu_parent extends CI_Controller
{
    
    public $CI;
    public $string_text;
    public $controller = 'c_menu_parent';
    public $controller_singkat = 'menuparent';
    public $table = 'r_menu';
    public $id = 'idr_menu';
    public $field = 'idr_menu,menu_name,menu_code,menu_controller,menu_icon,menu_status,menu_status_offline,menu_parent_status,menu_akses_name,menu_akses_code,menu_order,created_date,created_who,modified_date,modified_who';
    public $tombol_simpan = 'Simpan';
    public $tombol_kembali = 'Kembali';
    public $judul_list = 'List Menu Parent';
    public $judul_form = 'Form Menu Parent';
    public $judul_detail = 'Detail Menu Parent';
    public $folder = 'menu';
    // Semua Kolom yg ada di tabel R_menu
    public $idr_menu = 'idr_menu';
    public $menu_name = 'menu_name';
    public $menu_code = 'menu_code';
    public $menu_controller = 'menu_controller';
    public $menu_icon = 'menu_icon';
    public $menu_status = 'menu_status';
    public $menu_status_offline = 'menu_status_offline';
    public $menu_parent_status = 'menu_parent_status';
    public $menu_akses_code = 'menu_akses_code';
    public $menu_akses_name = 'menu_akses_name';
    public $menu_order = 'menu_order';
    public $idr_parent = 'idr_parent';
    public $created_date = 'created_date';
    public $created_who = 'created_who';
    public $modified_date = 'modified_date';
    public $modified_who = 'modified_who';
    
    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('m_data');
        $this->load->model('m_menu');
        $this->load->library('form_validation');        
        $this->load->library('datatables');
        $this->CI = & get_instance();
        $this->CI->config->load('string_text');
        $this->string_text = $this->CI->config->item('text');
    }
    
    public function index()
    {
        // Mode Log Aktif
        if(getInfoAPP('flag_log') == 'ON') {
            // Isi Tabel Log Menu
            $this->m_data->insert_log('Menu List Menu Parent '.date('Y-m-d H:m:s'),$this->session->userdata('id'));
        }
        $data['judul_list'] = $this->judul_list;
        $data['controller'] = $this->controller;
        $data['field'] = $this->field;
        $data['table'] = $this->table;
        $data['id'] = $this->id;
        $data['flag_tambah'] = TRUE;
        $data['flag_export_excel'] = FALSE;
        $data['flag_export_word'] = FALSE;
        $data['flag_edit'] = TRUE;
        $data['flag_detail'] = TRUE;
        $data['flag_delete'] = TRUE;
        $this->template->load('template',$this->folder.'/v_menu_parent_list',$data);
    } 
    
    public function json() {
        header('Content-Type: application/json');
        $where = 'idr_parent IS NULL';
        echo $this->m_menu->json_where($this->field,$this->table,$this->id,$this->controller,$where);
    }
    
    public function tambah() 
    {
        // Mode Log Aktif
        if(getInfoAPP('flag_log') == 'ON') {
            // Isi Tabel Log Menu
            $this->m_data->insert_log('Form Tambah Menu '.date('Y-m-d H:m:s'),$this->session->userdata('id'));
        }
        $data = array(
            'judul_form' => $this->judul_form,
            'tombol_simpan' => $this->tombol_simpan,
            'tombol_kembali' => $this->tombol_kembali,
            'controller_singkat' => $this->controller_singkat,
            'id' => $this->id,
            'aksi' => site_url($this->controller.'/aksi_tambah'),
	    'id_value' => set_value($this->idr_menu),
            $this->menu_name => set_value($this->menu_name),
            $this->menu_code => set_value($this->menu_code),
            $this->menu_icon => set_value($this->menu_icon),
            $this->menu_status => set_value($this->menu_status),
            $this->menu_status_offline => set_value($this->menu_status_offline),
            $this->menu_parent_status => set_value($this->menu_parent_status),
            $this->menu_order => set_value($this->menu_order),
            $this->menu_akses_name => set_value($this->menu_akses_name),
            $this->menu_akses_code => set_value($this->menu_akses_code),
	);
        $this->db->order_by('id', 'ASC');
        $data['grouplist'] = $this->m_data->get_data('aauth_groups')->result();
        $this->template->load('template',$this->folder.'/v_menu_parent_form', $data);
    }
    
    public function aksi_tambah() 
    {
        $this->form_validation->set_rules($this->menu_name, 'Nama Menu', 'trim|required', array('required' => 'Nama Menu'.$this->string_text['harus_diisi']));
	$this->form_validation->set_rules($this->menu_code, 'Kode Menu', 'trim|is_unique[r_menu.menu_code]|required', array('is_unique' => 'Menu Kode'.$this->string_text['sudah_ada'], 'required' => 'Menu Kode'.$this->string_text['harus_diisi']));
        $this->form_validation->set_rules($this->menu_icon, 'Icon Menu', 'required',array('required' => 'Icon Menu'.$this->string_text['harus_diisi']));
	$this->form_validation->set_error_delimiters('<span class="text-danger"><b>', '</b></span>');
        $row_menu_order = $this->db->query("SELECT * FROM r_menu WHERE idr_parent IS NULL ORDER BY menu_order DESC LIMIT 1")->row_array();
        $menu_order_last = $row_menu_order['menu_order']+1;
        if ($this->form_validation->run() == FALSE) {
            $this->tambah();
        } else {
            $data = array(
                $this->menu_name => $this->input->post($this->menu_name,TRUE),
		$this->menu_code => strtoupper($this->input->post($this->menu_code,TRUE)),
                $this->menu_icon => strtolower($this->input->post($this->menu_icon,TRUE)),
                $this->menu_status => $this->input->post($this->menu_status,TRUE),
                $this->menu_status_offline => $this->input->post($this->menu_status_offline,TRUE),
                $this->menu_parent_status => 'YA',
                $this->menu_order => $menu_order_last,
                $this->idr_parent => NULL,
                $this->created_who => strtoupper($this->session->userdata('user_name')),
	    );
            $this->m_data->insert_data($data, $this->table);
            // Update T_menu
            $id = $this->db->insert_id();
            
            $this->m_data->delete_data('idr_menu = '.$id,'r_menu_akses');
            $group_jumlah = $this->input->post('group_jumlah');
            for($i = 1; $i <= $group_jumlah; $i++)
            {
               $group = $this->input->post('id'.$i);
               $name = $this->input->post('name'.$i);
               $title = $this->input->post('title'.$i);
               if (!empty($group))
               {
                    $this->db->set('idr_menu', $id);
                    $this->db->set('idr_role', $group);
                    $this->db->set('role_name', $name);
                    $this->db->set('role_code', $title);
                    $this->db->insert('r_menu_akses');
               }
            }
            
            $akses = $this->m_data->get_data_filter('r_menu_akses','idr_menu = '.$id)->result_array();
            $insert_akses_name = implode(', ', array_column($akses, 'role_name'));
            $insert_akses_code = implode(', ', array_column($akses, 'role_code'));
            $menu_akses = array(
                $this->menu_akses_name => $insert_akses_name,
                $this->menu_akses_code => $insert_akses_code,
            );
            $this->db->where($this->id, $id);
            $this->db->update($this->table,$menu_akses);
            
            // Mode Log Aktif
            if(getInfoAPP('flag_log') == 'ON') {
                // Isi Tabel Log Menu
                $this->m_data->insert_log('Tambah Menu '.$id.'-'.date('Y-m-d H:m:s'),$this->session->userdata('id'));
            }
            $this->session->set_flashdata('message', $this->string_text['simpan_data_berhasil']);
            redirect(site_url($this->controller_singkat));
        }
    }
    
    public function edit($id) 
    {
        // Mode Log Aktif
        if(getInfoAPP('flag_log') == 'ON') {
            // Isi Tabel Log Menu
            $this->m_data->insert_log('Form Edit Menu '.date('Y-m-d H:m:s'),$this->session->userdata('id'));
        }
        $row = $this->m_data->get_by_id($this->table, $this->id,$id);
        if ($row) {
            $data = array(
                'judul_form' => $this->judul_form,
                'tombol_simpan' => $this->tombol_simpan,
                'tombol_kembali' => $this->tombol_kembali,
                'controller_singkat' => $this->controller_singkat,
                'id' => $this->id,
                'aksi' => site_url($this->controller.'/aksi_edit'),
                'id_value' => set_value('id_value', $row->idr_menu),
                $this->menu_name => set_value($this->menu_name, $row->menu_name),
		$this->menu_code => set_value($this->menu_code, $row->menu_code),
                $this->menu_icon => set_value($this->menu_icon, $row->menu_icon),
		$this->menu_status => set_value($this->menu_status, $row->menu_status),
                $this->menu_status_offline => set_value($this->menu_status_offline, $row->menu_status_offline),
                $this->menu_akses_name => set_value($this->menu_akses_name, $row->menu_akses_name),
                $this->menu_akses_code => set_value($this->menu_akses_code, $row->menu_akses_code),
	    );
            $this->db->order_by('id', 'ASC');
            $data['grouplist'] = $this->m_data->get_data('aauth_groups')->result();
            $this->template->load('template',$this->folder.'/v_menu_parent_form', $data);
        } else {
            $this->session->set_flashdata('message', $this->string_text['data_tidak_ada']);
            redirect(site_url($this->controller_singkat));
        }
    }
    
    public function aksi_edit() 
    {
        $idr_menu = $this->input->post($this->id, TRUE);
        $row = $this->m_data->get_by_id($this->table, $this->id,$idr_menu);
        $this->form_validation->set_rules($this->menu_name, 'Nama Menu', 'trim|required', array('required' => 'Nama Menu'.$this->string_text['harus_diisi']));
        if($row->menu_code != $this->input->post($this->menu_code)) {
            $this->form_validation->set_rules($this->menu_code, 'Kode Menu', 'trim|is_unique[r_menu.menu_code]|required', array('is_unique' => 'Menu Kode'.$this->string_text['sudah_ada'], 'required' => 'Menu Kode'.$this->string_text['harus_diisi']));
        } else {
            $this->form_validation->set_rules($this->menu_code, 'Kode Menu', 'trim|required', array('required' => 'Menu Kode'.$this->string_text['harus_diisi']));
        }
	$this->form_validation->set_rules($this->menu_icon, 'Icon Menu', 'required',array('required' => 'Icon Menu'.$this->string_text['harus_diisi']));
	$this->form_validation->set_error_delimiters('<span class="text-danger"><b>', '</b></span>');
        if ($this->form_validation->run() == FALSE) {
            $this->edit($this->input->post($this->id, TRUE));
        } else {
            $data = array(
                $this->idr_menu => $this->input->post($this->id,TRUE),
                $this->menu_name => $this->input->post($this->menu_name,TRUE),
		$this->menu_code => strtoupper($this->input->post($this->menu_code,TRUE)),
                $this->menu_icon => strtolower($this->input->post($this->menu_icon,TRUE)),
                $this->menu_status => $this->input->post($this->menu_status,TRUE),
                $this->menu_status_offline => $this->input->post($this->menu_status_offline,TRUE),
                $this->modified_who => strtoupper($this->session->userdata('user_name')),
	    );
            $this->m_data->update_data($this->id.' = '.$idr_menu, $data, $this->table);
            // Update T_menu
            $id = $this->input->post($this->id,TRUE);
            
            $this->m_data->delete_data('idr_menu = '.$id,'r_menu_akses');
            $group_jumlah = $this->input->post('group_jumlah');
            for($i = 1; $i <= $group_jumlah; $i++)
            {
               $group = $this->input->post('id'.$i);
               $name = $this->input->post('name'.$i);
               $title = $this->input->post('title'.$i);
               if (!empty($group))
               {
                    $this->db->set('idr_menu', $id);
                    $this->db->set('idr_role', $group);
                    $this->db->set('role_name', $name);
                    $this->db->set('role_code', $title);
                    $this->db->insert('r_menu_akses');
               }
            }
            
            $akses = $this->m_data->get_data_filter('r_menu_akses','idr_menu = '.$id)->result_array();
            $insert_akses_name = implode(', ', array_column($akses, 'role_name'));
            $insert_akses_code = implode(', ', array_column($akses, 'role_code'));
            $menu_akses = array(
                $this->menu_akses_name => $insert_akses_name,
                $this->menu_akses_code => $insert_akses_code,
            );
            $this->db->where($this->id, $id);
            $this->db->update($this->table,$menu_akses);
            
            // Mode Log Aktif
            if(getInfoAPP('flag_log') == 'ON') {
                // Isi Tabel Log Menu
                $this->m_data->insert_log('Edit Menu '.$id.'-'.date('Y-m-d H:m:s'),$this->session->userdata('id'));
            }
            $this->session->set_flashdata('message', $this->string_text['ubah_data_berhasil']);
            redirect(site_url($this->controller_singkat));
        }
    }
    
    public function hapus($id) 
    {
        $row = $this->m_data->get_by_id($this->table, $this->id,$id);
        if ($row) {
            // Mode Log Aktif
            if(getInfoAPP('flag_log') == 'ON') {
                // Isi Tabel Log Menu
                $this->m_data->insert_log('Delete Menu '.$id.'-'.date('Y-m-d H:m:s'),$this->session->userdata('id'));
            }
            $this->m_data->delete_data($this->id.' = '.$id,$this->table);
            $this->session->set_flashdata('message', $this->string_text['hapus_data_berhasil']);
            redirect(site_url($this->controller_singkat));
        } else {
            $this->session->set_flashdata('message', $this->string_text['data_tidak_ada']);
            redirect(site_url($this->controller_singkat));
        }
    }
    
    public function detail($id) 
    {
        $row = $this->m_data->get_by_id($this->table, $this->id,$id);
        // Mode Log Aktif
        if(getInfoAPP('flag_log') == 'ON') {
            // Isi Tabel Log Menu
            $this->m_data->insert_log('Detail Menu Parent'.$row->menu_name.'-'.$row->menu_code.'-'.date('Y-m-d H:m:s'),$this->session->userdata('id'));
        }
        $data = array(
            'judul_form' => $this->judul_detail,
            'tombol_kembali' => $this->tombol_kembali,
            'controller' => $this->controller,
            'controller_singkat' => $this->controller_singkat,
            'folder' => $this->folder,
            'id' => $this->id,
	    $this->idr_menu => $row->idr_menu,
            $this->menu_name => $row->menu_name,
            $this->menu_code => $row->menu_code,
            $this->menu_controller => $row->menu_controller,
            $this->menu_icon => $row->menu_icon,
            $this->menu_status => $row->menu_status,
            $this->menu_status_offline => $row->menu_status_offline,
            $this->menu_parent_status => $row->menu_parent_status,
            $this->menu_akses_name => $row->menu_akses_name,
            $this->menu_akses_code => $row->menu_akses_code,
            $this->menu_order => $row->menu_order,
            $this->idr_parent => $row->idr_parent,
            $this->created_date => $row->created_date,
            $this->created_who => $row->created_who,
            $this->modified_date => $row->modified_date,
            $this->modified_who => $row->modified_who,
        );
        $this->template->load('template',$this->folder.'/v_menu_parent_detail',$data);
    }
    
    public function up($idr_menu) 
    {
        $row = $this->m_data->get_by_id($this->table, $this->id,$idr_menu);
        $menu_order = $row->menu_order;
        $menu_prev = $row->menu_order-1;
        $row_sebelum = $this->db->query("SELECT * FROM r_menu WHERE idr_parent IS NULL AND menu_order = $menu_prev")->row_array();
        $idr_menu_sebelum = $row_sebelum['idr_menu'];
        $menu_order_sebelum = $row_sebelum['menu_order'];
        $row_sebelum_count = $this->db->query("SELECT COUNT(*) as hasil FROM r_menu WHERE idr_parent IS NULL AND menu_order = $menu_prev")->row_array();
        if($row_sebelum_count['hasil'] != 0) {
            $data = array(
                $this->menu_order => $menu_order_sebelum,
            );
            $this->m_data->update_data($this->id.' = '.$idr_menu,$data,$this->table);
            $data_sebelum = array(
                $this->menu_order => $menu_order,
            );
            $this->m_data->update_data($this->id.' = '.$idr_menu_sebelum,$data_sebelum,$this->table);
            // Mode Log Aktif
            if(getInfoAPP('flag_log') == 'ON') {
                // Isi Tabel Log Menu
                $this->m_data->insert_log('Up Menu Parent '.$row->menu_name.'-'.$row->menu_code.'-'.date('Y-m-d H:m:s'),$this->session->userdata('id'));
            }
        } else {
            // Mode Log Aktif
            if(getInfoAPP('flag_log') == 'ON') {
                // Isi Tabel Log Menu
                $this->m_data->insert_log('Up Menu Parent Paling Atas '.$row->menu_name.'-'.$row->menu_code.'-'.date('Y-m-d H:m:s'),$this->session->userdata('id'));
            }
        }
        redirect(site_url($this->controller_singkat));
    }
    
    public function down($idr_menu) 
    {
        $row = $this->m_data->get_by_id($this->table, $this->id,$idr_menu);
        $menu_order = $row->menu_order;
        $menu_next = $row->menu_order+1;
        $row_sesudah = $this->db->query("SELECT * FROM r_menu WHERE idr_parent IS NULL AND menu_order = $menu_next")->row_array();
        $idr_menu_sesudah = $row_sesudah['idr_menu'];
        $menu_order_sebelum = $row_sesudah['menu_order'];
        $row_sesudah_count = $this->db->query("SELECT COUNT(*) as hasil FROM r_menu WHERE idr_parent IS NULL AND menu_order = $menu_next")->row_array();
        if($row_sesudah_count['hasil'] != 0) {
            $data = array(
                $this->menu_order => $menu_order_sebelum,
            );
            $this->m_data->update_data($this->id.' = '.$idr_menu,$data,$this->table);
            $data_sebelum = array(
                $this->menu_order => $menu_order,
            );
            $this->m_data->update_data($this->id.' = '.$idr_menu_sesudah,$data_sebelum,$this->table);
            // Mode Log Aktif
            if(getInfoAPP('flag_log') == 'ON') {
                // Isi Tabel Log Menu
                $this->m_data->insert_log('Down Menu Parent '.$row->menu_name.'-'.$row->menu_code.'-'.date('Y-m-d H:m:s'),$this->session->userdata('id'));
            }
        } else {
            // Mode Log Aktif
            if(getInfoAPP('flag_log') == 'ON') {
                // Isi Tabel Log Menu
                $this->m_data->insert_log('Down Menu Parent Paling Bawah '.$row->menu_name.'-'.$row->menu_code.'-'.date('Y-m-d H:m:s'),$this->session->userdata('id'));
            }
        }
        redirect(site_url($this->controller_singkat));
    }
    
}
