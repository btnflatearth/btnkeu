<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of C_user
 *
 * @author Dicky Satria Utama
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class C_user extends CI_Controller
{
    public $CI;
    public $string_text;
    public $controller = 'C_user';
    public $controller_singkat = 'user';
    public $table = 'aauth_users';
    public $view = 'v_users';
    public $id = 'id';
    public $field = 'id,email,name,grup,status,last_login,akses_satker';
    public $tombol_simpan = 'Simpan';
    public $tombol_kembali = 'Kembali';
    public $judul_list = 'List User';
    public $judul_form = 'Form User';
    public $judul_detail = 'Detail User';
    public $folder = 'user';
    // Semua Kolom yg ada di tabel T_user
    public $id_key = 'id';
    public $email = 'email';
    public $name = 'name';
    public $pass = 'pass';
    public $grup = 'grup';
    public $status = 'status';
    
    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('m_data');
        $this->load->library('form_validation');        
        $this->load->library('datatables');
        $this->CI = & get_instance();
        $this->CI->config->load('string_text');
        $this->string_text = $this->CI->config->item('text');
    }
    
    public function index()
    {
        // Mode Log Aktif
        if(getInfoAPP('flag_log') == 'ON') {
            // Isi Tabel Log User
            $this->m_data->insert_log('Menu List User '.date('Y-m-d H:m:s'),$this->session->userdata('id'));
        }
        $data['judul_list'] = $this->judul_list;
        $data['controller'] = $this->controller;
        $data['field'] = $this->field;
        $data['table'] = $this->table;
        $data['id'] = $this->id;
        $data['flag_tambah'] = TRUE;
        $data['flag_export_excel'] = FALSE;
        $data['flag_export_word'] = FALSE;
        $data['flag_edit'] = TRUE;
        $data['flag_detail'] = TRUE;
        $data['flag_delete'] = TRUE;
        $this->template->load('template',$this->folder.'/v_user_list',$data);
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->m_data->json($this->field,$this->view,$this->id_key,$this->controller);
    }
    
    public function tambah() 
    {
        // Mode Log Aktif
        if(getInfoAPP('flag_log') == 'ON') {
            // Isi Tabel Log User
            $this->m_data->insert_log('Form Tambah Data User '.date('Y-m-d H:m:s'),$this->session->userdata('id'));
        }
        $data = array(
            'judul_form' => $this->judul_form,
            'tombol_simpan' => $this->tombol_simpan,
            'tombol_kembali' => $this->tombol_kembali,
            'controller' => $this->controller,
            'id' => $this->id,
            'aksi' => site_url($this->controller.'/aksi_tambah'),
	    'id_value' => set_value($this->id_key),
            $this->email => set_value($this->email),
            $this->name => set_value($this->name),
	);
        $this->template->load('template',$this->folder.'/v_user_form', $data);
    }
    
    public function aksi_tambah() 
    {
        $this->form_validation->set_rules($this->email, 'Email', 'trim|is_unique[aauth_users.email]|required', array('is_unique' => 'Email'.$this->string_text['sudah_ada'], 'required' => 'Email'.$this->string_text['harus_diisi']));
        $this->form_validation->set_rules($this->name, 'Username', 'trim|is_unique[aauth_users.name]|required', array('is_unique' => 'Username'.$this->string_text['sudah_ada'], 'required' => 'Username'.$this->string_text['harus_diisi']));
	$this->form_validation->set_error_delimiters('<span class="text-danger"><b>', '</b></span>');
        if ($this->form_validation->run() == FALSE) {
            $this->tambah();
        } else {
            $data = array(
                $this->email => $this->input->post($this->email,TRUE),
                $this->name => $this->input->post($this->name,TRUE),
                $this->pass => md5(getInfoAPP('default_password')),
                //$this->created_by => strtoupper($this->session->userdata($this->email)),
	    );
            $this->m_data->insert_data($data, $this->table);
            $id = $this->db->insert_id();
            
            $this->db->set('user_id', $id);
            $this->db->set('group_id', 4);
            $this->db->insert('aauth_user_to_group');
            
            // Mode Log Aktif
            if(getInfoAPP('flag_log') == 'ON') {
                // Isi Tabel Log User
                $this->m_data->insert_log('Tambah Data User '.$id.'-'.date('Y-m-d H:m:s'),$this->session->userdata('id'));
            }
            $this->session->set_flashdata('message', $this->string_text['simpan_data_berhasil']);
            redirect(site_url($this->controller));
        }
    }
    
    public function edit($id) 
    {
        // Mode Log Aktif
        if(getInfoAPP('flag_log') == 'ON') {
            // Isi Tabel Log User
            $this->m_data->insert_log('Form Edit User '.date('Y-m-d H:m:s'),$this->session->userdata('id'));
        }
        $row = $this->m_data->get_by_id($this->table, $this->id,$id);
        if ($row) {
            $data = array(
                'judul_form' => $this->judul_form,
                'tombol_simpan' => $this->tombol_simpan,
                'tombol_kembali' => $this->tombol_kembali,
                'controller' => $this->controller,
                'id' => $this->id,
                'aksi' => site_url($this->controller.'/aksi_edit'),
                'id_value' => set_value($this->id_key, $row->id),
                $this->email => set_value($this->email, $row->email),
                $this->name => set_value($this->name, $row->name),
	    );
            $this->template->load('template',$this->folder.'/v_user_form', $data);
        } else {
            $this->session->set_flashdata('message', $this->string_text['data_tidak_ada']);
            redirect(site_url($this->controller));
        }
    }
    
    public function aksi_edit() 
    {
        $id = $this->input->post($this->id, TRUE);
        $row = $this->m_data->get_by_id($this->table, $this->id,$id);
        if($row->email != $this->input->post($this->email)) {
            $this->form_validation->set_rules($this->user_email, 'Email', 'trim|is_unique[aauth_users.email]|required', array('is_unique' => 'Email'.$this->string_text['sudah_ada'], 'required' => 'Email'.$this->string_text['harus_diisi']));
        } else {
            $this->form_validation->set_rules($this->user_email, 'Email', 'trim|required', array('required' => 'Email'.$this->string_text['harus_diisi']));
        }
        if($row->name != $this->input->post($this->name)) {
            $this->form_validation->set_rules($this->name, 'Username', 'trim|is_unique[aauth_users.name]|required', array('is_unique' => 'Username'.$this->string_text['sudah_ada'], 'required' => 'Username'.$this->string_text['harus_diisi']));
        } else {
            $this->form_validation->set_rules($this->name, 'Username', 'trim|required', array('required' => 'Username'.$this->string_text['harus_diisi']));
        }
	$this->form_validation->set_error_delimiters('<span class="text-danger"><b>', '</b></span>');
        if ($this->form_validation->run() == FALSE) {
            $this->edit($this->input->post($this->id, TRUE));
        } else {
            $data = array(
                $this->id => $this->input->post($this->id,TRUE),
                $this->email => $this->input->post($this->email,TRUE),
                $this->name => $this->input->post($this->name,TRUE),
                //$this->updated_by => strtoupper($this->session->userdata($this->email)),
	    );
            $this->m_data->update_data($this->id.' = '.$id, $data, $this->table);
            // Update T_user
            $id = $this->input->post($this->id,TRUE);
            // Mode Log Aktif
            if(getInfoAPP('flag_log') == 'ON') {
                // Isi Tabel Log User
                $this->m_data->insert_log('Edit User '.$id.'-'.date('Y-m-d H:m:s'),$this->session->userdata('id'));
            }
            $this->session->set_flashdata('message', $this->string_text['ubah_data_berhasil']);
            redirect(site_url($this->controller));
        }
    }
    
    public function hapus($id) 
    {
        $row = $this->m_data->get_by_id($this->table, $this->id,$id);
        if ($row) {
            // Mode Log Aktif
            if(getInfoAPP('flag_log') == 'ON') {
                // Isi Tabel Log User
                $this->m_data->insert_log('Delete User '.$id.'-'.date('Y-m-d H:m:s'),$this->session->userdata('id'));
            }
            $this->m_data->delete_data($this->id.' = '.$id,$this->table);
            $this->session->set_flashdata('message', $this->string_text['hapus_data_berhasil']);
            redirect(site_url($this->controller));
        } else {
            $this->session->set_flashdata('message', $this->string_text['data_tidak_ada']);
            redirect(site_url($this->controller));
        }
    }
    
    public function detail($id) 
    {
        // Mode Log Aktif
        if(getInfoAPP('flag_log') == 'ON') {
            // Isi Tabel Log User
            $this->m_data->insert_log('Menu Detail User '.date('Y-m-d H:m:s'),$this->session->userdata('id'));
        }
        //$row = $this->m_data->get_by_id($this->view, $this->id,$id);
        $data = array(
            'judul' => 'Detail User',
            'id' => $id,
            'controller' => $this->controller,
            'tombol_kembali' => $this->tombol_kembali,
            'judul_akses_unit' => 'Hak Akses Unit',
            'judul_log' => 'Log Kegiatan User'
        );
        $this->template->load('template','user/v_user_detail',$data);
    }
    
    public function edit_role($id) 
    {
        // Mode Log Aktif
        if(getInfoAPP('flag_log') == 'ON') {
            // Isi Tabel Log User
            $this->m_data->insert_log('Form Edit Role '.date('Y-m-d H:m:s'),$this->session->userdata('id'));
        }
        $row = $this->m_data->get_by_id('aauth_user_to_group', 'user_id',$id);
        if ($row) {
            $data = array(
                'judul_form' => 'Form Edit Role',
                'tombol_simpan' => $this->tombol_simpan,
                'tombol_kembali' => $this->tombol_kembali,
                'controller' => $this->controller,
                'id' => 'user_id',
                'aksi' => site_url($this->controller.'/aksi_edit_role'),
                'user_id' => set_value('user_id', $row->user_id),
                'group_id' => set_value('group_id', $row->group_id)
	    );
            $data['rolelist'] = $this->m_data->get_data('aauth_groups')->result();
            $this->template->load('template',$this->folder.'/v_user_role_form', $data);
        } else {
            $this->session->set_flashdata('message', $this->string_text['data_tidak_ada']);
            redirect(site_url($this->controller.'/detail/'.$id));
        }
    }
    
    public function aksi_edit_role() 
    {
        $user_id = $this->input->post('user_id', TRUE);
        $this->form_validation->set_rules('group_id', 'Role', 'required', array('required' => 'Role '.$this->string_text['harus_diisi']));
	$this->form_validation->set_error_delimiters('<span class="text-danger"><b>', '</b></span>');
        if ($this->form_validation->run() == FALSE) {
            $this->edit_role($this->input->post('user_id', TRUE));
        } else {
            $data = array(
                'user_id' => $this->input->post('user_id',TRUE),
                'group_id' => $this->input->post('group_id',TRUE),
                //$this->updated_by => strtoupper($this->session->userdata($this->email)),
	    );
            $this->m_data->update_data('user_id'.' = '.$user_id, $data, 'aauth_user_to_group');
            // Update T_user
            $id = $this->input->post('user_id',TRUE);
            // Mode Log Aktif
            if(getInfoAPP('flag_log') == 'ON') {
                // Isi Tabel Log User
                $this->m_data->insert_log('Edit User Role '.$id.'-'.date('Y-m-d H:m:s'),$this->session->userdata('id'));
            }
            $this->session->set_flashdata('message', $this->string_text['ubah_data_berhasil']);
            redirect(site_url($this->controller.'/detail/'.$id));
        }
    }
    
    public function user_off($id) 
    {
        $data = array(
            'banned' => 1,
            //$this->updated_by => strtoupper($this->session->userdata($this->email)),
        );
        $this->m_data->update_data($this->id.' = '.$id, $data, $this->table);
        redirect(site_url($this->controller.'/detail/'.$id));
    }
    
    public function user_on($id) 
    {
        $data = array(
            'banned' => 0,
            //$this->updated_by => strtoupper($this->session->userdata($this->email)),
        );
        $this->m_data->update_data($this->id.' = '.$id, $data, $this->table);
        redirect(site_url($this->controller.'/detail/'.$id));
    }
    
    public function json_akses_unit($id_key) {
        header('Content-Type: application/json');
        $field = 'idt_user_satker,id,nama_satker';
        $view = 'v_t_user_satker';
        $id = 'idt_user_satker';
        $controller = $this->controller;
        $where = 'id = '.$id_key;
        echo $this->m_data->json_where_var($field,$view,$id,$controller,$where,'_akses_unit');
    }
    
    public function tambah_akses_unit($id) 
    {
        // Mode Log Aktif
        if(getInfoAPP('flag_log') == 'ON') {
            // Isi Tabel Log User
            $this->m_data->insert_log('Form Tambah Data User Akses '.date('Y-m-d H:m:s'),$this->session->userdata('id'));
        }
        $data = array(
            'judul_form' => 'Form Tambah User Akses',
            'tombol_simpan' => $this->tombol_simpan,
            'tombol_kembali' => $this->tombol_kembali,
            'controller' => $this->controller,
            'id_key' => 'idt_user_satker',
            'aksi' => site_url($this->controller.'/aksi_tambah_akses_unit'),
	    'idt_user_satker' => set_value('idt_user_satker'),
            'id' => set_value('id',$id),
            'idr_satker' => set_value('idr_satker'),
	);
        $data['unitlist'] = $this->m_data->get_data('r_satker')->result();
        $this->template->load('template',$this->folder.'/v_user_akses_unit_form', $data);
    }
    
    public function aksi_tambah_akses_unit() 
    {
        $id = $this->input->post($this->id,TRUE);
        $this->form_validation->set_rules('idr_satker', 'Nama Satker', 'required|callback_cek_user_akses_double', array('required' => 'Nama Satker '.$this->string_text['harus_diisi'], 'cek_user_akses_double' => 'Data User Akses '.$this->string_text['sudah_ada']));
	$this->form_validation->set_error_delimiters('<span class="text-danger"><b>', '</b></span>');
        if ($this->form_validation->run() == FALSE) {
            $this->tambah_akses_unit($id);
        } else {
            $data = array(
                'id' => $this->input->post('id',TRUE),
                'idr_satker' => $this->input->post('idr_satker',TRUE),
                //$this->created_by => strtoupper($this->session->userdata($this->email)),
	    );
            $this->m_data->insert_data($data, 't_user_satker');
            $id_key = $this->db->insert_id();
            // Mode Log Aktif
            if(getInfoAPP('flag_log') == 'ON') {
                // Isi Tabel Log User
                $this->m_data->insert_log('Tambah Data User Akses '.$id_key.'-'.date('Y-m-d H:m:s'),$this->session->userdata('id'));
            }
            $this->session->set_flashdata('message', $this->string_text['simpan_data_berhasil']);
            redirect(site_url($this->controller.'/detail/'.$id));
        }
    }
    
    public function edit_akses_unit($id) 
    {
        // Mode Log Aktif
        if(getInfoAPP('flag_log') == 'ON') {
            // Isi Tabel Log User
            $this->m_data->insert_log('Form Edit User Akses '.date('Y-m-d H:m:s'),$this->session->userdata('id'));
        }
        $row = $this->m_data->get_by_id('t_user_satker', 'idt_user_satker',$id);
        if ($row) {
            $data = array(
                'judul_form' => $this->judul_form,
                'tombol_simpan' => $this->tombol_simpan,
                'tombol_kembali' => $this->tombol_kembali,
                'controller' => $this->controller,
                'id_key' => 'idt_user_satker',
                'aksi' => site_url($this->controller.'/aksi_edit_akses_unit'),
                'idt_user_satker' => set_value('idt_user_satker', $row->idt_user_satker),
                'id' => set_value('idr_user', $row->id),
		'idr_satker' => set_value('idr_satker', $row->idr_satker)
	    );
            $data['unitlist'] = $this->m_data->get_data('r_satker')->result();
            $this->template->load('template',$this->folder.'/v_user_akses_unit_form', $data);
        } else {
            $this->session->set_flashdata('message', $this->string_text['data_tidak_ada']);
            redirect(site_url($this->controller.'/detail/'.$row->id));
        }
    }
    
    public function aksi_edit_akses_unit() 
    {
        $idt_user_satker = $this->input->post('idt_user_satker', TRUE);
        $row = $this->m_data->get_by_id('t_user_satker', 'idt_user_satker',$idt_user_satker);
        if(($row->id.$row->idr_satker) != ($this->input->post('id').$this->input->post('idr_satker'))) {
            $this->form_validation->set_rules('idr_satker', 'Nama Satker', 'required|callback_cek_user_akses_double', array('required' => 'Nama Satker '.$this->string_text['harus_diisi'], 'cek_user_akses_double' => 'Data User Akses '.$this->string_text['sudah_ada']));
        } else {
            $this->form_validation->set_rules('idr_satker', 'Nama Satker', 'required', array('required' => 'Nama Satker '.$this->string_text['harus_diisi']));
        }
        $this->form_validation->set_error_delimiters('<span class="text-danger"><b>', '</b></span>');
        if ($this->form_validation->run() == FALSE) {
            $this->edit_akses_unit($this->input->post('idt_user_akses_unit', TRUE));
        } else {
            $data = array(
                'idt_user_satker' => $this->input->post('idt_user_satker',TRUE),
                'id' => $this->input->post('id',TRUE),
                'idr_satker' => $this->input->post('idr_satker',TRUE),
                //$this->updated_by => strtoupper($this->session->userdata($this->email)),
	    );
            $this->m_data->update_data('idt_user_satker'.' = '.$idt_user_satker, $data, 't_user_satker');
            // Update T_user
            $id = $this->input->post('idt_user_satker',TRUE);
            // Mode Log Aktif
            if(getInfoAPP('flag_log') == 'ON') {
                // Isi Tabel Log User
                $this->m_data->insert_log('Edit User Akses'.$id.'-'.date('Y-m-d H:m:s'),$this->session->userdata('id'));
            }
            $this->session->set_flashdata('message', $this->string_text['ubah_data_berhasil']);
            redirect(site_url($this->controller.'/detail/'.$this->input->post('id',TRUE)));
        }
    }
    
    public function hapus_akses_unit($id) 
    {
        $row = $this->m_data->get_by_id('t_user_satker', 'idt_user_satker',$id);
        if ($row) {
            // Mode Log Aktif
            if(getInfoAPP('flag_log') == 'ON') {
                // Isi Tabel Log User
                $this->m_data->insert_log('Delete User Akses '.$id.'-'.date('Y-m-d H:m:s'),$this->session->userdata('id'));
            }
            $this->m_data->delete_data('idt_user_satker'.' = '.$id,'t_user_satker');
            $this->session->set_flashdata('message', $this->string_text['hapus_data_berhasil']);
            redirect(site_url($this->controller.'/detail/'.$row->id));
        } else {
            $this->session->set_flashdata('message', $this->string_text['data_tidak_ada']);
            redirect(site_url($this->controller.'/detail/'.$row->id));
        }
    }
    
    function cek_user_akses_double() {
        $id = $this->input->post('id');
        $idr_satker = $this->input->post('idr_satker');
        $this->db->select('idt_user_satker');
        $this->db->from('t_user_satker');
        //$this->db->like('email', $email);
        $this->db->where('id', $id);
        $this->db->where('idr_satker', $idr_satker);
        $query = $this->db->get();
        $num = $query->num_rows();
        if ($num > 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
    
    public function json_log($id_key) {
        header('Content-Type: application/json');
        $field = 'idt_log_user,id,name,log_user,created_date';
        $view = 't_log_user';
        $id = 'idt_log_user';
        $controller = $this->controller;
        $where = 'id = '.$id_key;
        echo $this->m_data->json_where($field,$view,$id,$controller,$where);
    }
    
}
