<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of C_user
 *
 * @author Dicky Satria Utama
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class C_ikpa_satker extends CI_Controller
{
    public $CI;
    public $string_text;
    public $controller = 'C_ikpa_satker';
    public $controller_singkat = 'ikpasatker';
    public $table = 't_ikpa_satker';
    public $view = 't_ikpa_satker';
    public $id = 'idt_ikpa_satker';
    public $field = 'idt_ikpa_satker,idr_tahun,idr_bulan,nama_bulan,nama_kanwil_bpn,nama_satker,flag_posting,nilai_akhir_ikpa';
    public $tombol_simpan = 'Simpan';
    public $tombol_kembali = 'Kembali';
    public $judul_list = 'LIST IKPA SATKER';
    public $judul_form = 'FORM IKPA SATKER';
    public $judul_detail = 'DETAIL IKPA SATKER';
    public $folder = 'ikpa_satker';
    // Semua Kolom yg ada di tabel T_user
    public $idt_ikpa_satker = 'idt_ikpa_satker';
    public $idr_tahun = 'idr_tahun';
    public $idr_bulan = 'idr_bulan';
    public $nama_bulan = 'nama_bulan';
    public $idr_kanwil_bpn = 'idr_kanwil_bpn';
    public $kdkanwil_bpn = 'kdkanwil_bpn';
    public $idr_satker = 'idr_satker';
    public $kdsatker = 'kdsatker';
    public $nama_kanwil_bpn = 'nama_kanwil_bpn';
    public $nama_satker = 'nama_satker';
    public $flag_posting = 'flag_posting';
    public $tanggal_posting = 'tanggal_posting';
    public $tanggal_dibuat = 'tanggal_dibuat';
    public $tanggal_diperbarui = 'tanggal_diperbarui';
    
    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('m_data');
        $this->load->library('form_validation');        
        $this->load->library('datatables');
        $this->CI = & get_instance();
        $this->CI->config->load('string_text');
        $this->string_text = $this->CI->config->item('text');
    }
    
    public function index()
    {
        // Mode Log Aktif
        if(getInfoAPP('flag_log') == 'ON') {
            // Isi Tabel Log User
            $this->m_data->insert_log('Menu List User '.date('Y-m-d H:m:s'),$this->session->userdata('id'));
        }
        $data['judul_list'] = $this->judul_list;
        $data['controller'] = $this->controller;
        $data['field'] = $this->field;
        $data['table'] = $this->table;
        $data['id'] = $this->id;
        $id_role = $this->session->userdata('id_role');
        if($id_role == 1 || $id_role == 2) {
            $data['flag_tambah'] = TRUE;
            $data['flag_edit'] = TRUE;
            $data['flag_delete'] = TRUE;
        } else {
            $data['flag_tambah'] = FALSE;
            $data['flag_edit'] = FALSE;
            $data['flag_delete'] = FALSE;
        }
        $data['flag_detail'] = TRUE;
        $data['flag_export_excel'] = FALSE;
        $data['flag_export_word'] = FALSE;
        $this->template->load('template',$this->folder.'/v_ikpa_satker_list',$data);
    } 
    
    public function json() {
        header('Content-Type: application/json');
        $id_role = $this->session->userdata('id_role');
        if($id_role == 1 || $id_role == 2) {
            
        } else {
            $where_in = $this->session->userdata('akses');
            $this->datatables->where_in('idr_satker', $where_in);
        }
        $this->db->order_by('idr_tahun', 'DESC');
        $this->db->order_by('idr_bulan', 'ASC');
        $this->db->order_by('nama_kanwil_bpn', 'ASC');
        echo $this->m_data->json_menu($this->field,$this->view,$this->id,$this->controller,'final');
    }
    
    public function tambah() 
    {
        // Mode Log Aktif
        if(getInfoAPP('flag_log') == 'ON') {
            // Isi Tabel Log User
            $this->m_data->insert_log('Form Tambah Data Ikpa Satker '.date('Y-m-d H:m:s'),$this->session->userdata('id'));
        }
        $data = array(
            'judul_form' => $this->judul_form,
            'tombol_simpan' => $this->tombol_simpan,
            'tombol_kembali' => $this->tombol_kembali,
            'controller' => $this->controller,
            'id' => $this->id,
            'aksi' => site_url($this->controller.'/aksi_tambah'),
	    'id_value' => set_value($this->id),
            $this->idr_tahun => set_value($this->idr_tahun),
            $this->idr_bulan => set_value($this->idr_bulan),
            $this->idr_satker => set_value($this->idr_satker),
	);
        $id_role = $this->session->userdata('id_role');
        $data['tahunlist'] = $this->m_data->get_data('r_tahun')->result();
        $data['bulanlist'] = $this->m_data->get_data('r_bulan')->result();
        if($id_role == 1 || $id_role == 2) {
            
        } else {
            $where_in = $this->session->userdata('akses');
            $this->db->where_in('idr_satker', $where_in);
        }
        $data['satkerlist'] = $this->m_data->get_data('r_satker')->result();
        $this->template->load('template',$this->folder.'/v_ikpa_satker_form', $data);
    }
    
    public function aksi_tambah() 
    {
        $this->form_validation->set_rules($this->idr_tahun, 'Tahun', 'required|callback_cek_ikpa_satker', array('required' => 'Tahun'.$this->string_text['harus_diisi'], 'cek_ikpa_satker' => 'Data Ikpa Dengan Tahun, Bulan, dan Satker yang diinput Sudah Ada !!!'));
        $this->form_validation->set_rules($this->idr_bulan, 'Bulan', 'required', array('required' => 'Bulan'.$this->string_text['harus_diisi']));
        $this->form_validation->set_rules($this->idr_satker, 'Satker', 'required', array('required' => 'Satker'.$this->string_text['harus_diisi']));
	$this->form_validation->set_error_delimiters('<span class="text-danger"><b>', '</b></span>');
        if ($this->form_validation->run() == FALSE) {
            $this->tambah();
        } else {
            $data = array(
                $this->idr_tahun => $this->input->post($this->idr_tahun,TRUE),
                $this->idr_bulan => $this->input->post($this->idr_bulan,TRUE),
                $this->idr_satker => $this->input->post($this->idr_satker,TRUE),
	    );
            $this->m_data->insert_data($data, $this->table);
            $id = $this->db->insert_id();
            
            // Mode Log Aktif
            if(getInfoAPP('flag_log') == 'ON') {
                // Isi Tabel Log User
                $this->m_data->insert_log('Tambah Data Ikpa Satker '.$id.'-'.date('Y-m-d H:m:s'),$this->session->userdata('id'));
            }
            $this->session->set_flashdata('message', $this->string_text['simpan_data_berhasil']);
            redirect(site_url($this->controller));
        }
    }
    
    public function edit($id) 
    {
        // Mode Log Aktif
        if(getInfoAPP('flag_log') == 'ON') {
            // Isi Tabel Log User
            $this->m_data->insert_log('Form Edit Ikpa Satker '.date('Y-m-d H:m:s'),$this->session->userdata('id'));
        }
        $row = $this->m_data->get_by_id($this->table, $this->id,$id);
        if ($row) {
            $data = array(
                'judul_form' => $this->judul_form,
                'tombol_simpan' => $this->tombol_simpan,
                'tombol_kembali' => $this->tombol_kembali,
                'controller' => $this->controller,
                'id' => $this->id,
                'aksi' => site_url($this->controller.'/aksi_edit'),
                'id_value' => set_value($this->id, $row->idt_ikpa_satker),
                $this->idr_tahun => set_value($this->idr_tahun, $row->idr_tahun),
                $this->idr_bulan => set_value($this->idr_bulan, $row->idr_bulan),
                $this->idr_satker => set_value($this->idr_satker, $row->idr_satker),
	    );
            $id_role = $this->session->userdata('id_role');
            $data['tahunlist'] = $this->m_data->get_data('r_tahun')->result();
            $data['bulanlist'] = $this->m_data->get_data('r_bulan')->result();
            if($id_role == 1 || $id_role == 2) {

            } else {
                $where_in = $this->session->userdata('akses');
                $this->db->where_in('idr_satker', $where_in);
            }
            $data['satkerlist'] = $this->m_data->get_data('r_satker')->result();
            $this->template->load('template',$this->folder.'/v_ikpa_satker_form', $data);
        } else {
            $this->session->set_flashdata('message', $this->string_text['data_tidak_ada']);
            redirect(site_url($this->controller));
        }
    }
    
    public function aksi_edit() 
    {
        $idt_ikpa_satker = $this->input->post($this->id, TRUE);
        $row = $this->m_data->get_by_id($this->table, $this->id,$idt_ikpa_satker);
        if(($row->idr_tahun.$row->idr_bulan.$row->idr_satker) == ($this->input->post($this->idr_tahun).$this->input->post($this->idr_bulan).$this->input->post($this->idr_satker))) {
            $this->form_validation->set_rules($this->idr_tahun, 'Tahun', 'required', array('required' => 'Tahun'.$this->string_text['harus_diisi']));
        } else {
            $this->form_validation->set_rules($this->idr_tahun, 'Tahun', 'required|callback_cek_ikpa_satker', array('required' => 'Tahun'.$this->string_text['harus_diisi'], 'cek_ikpa_satker' => 'Data Ikpa Dengan Tahun, Bulan, dan Satker yang diinput Sudah Ada !!!'));
        }
        $this->form_validation->set_rules($this->idr_bulan, 'Bulan', 'required', array('required' => 'Bulan'.$this->string_text['harus_diisi']));
        $this->form_validation->set_rules($this->idr_satker, 'Satker', 'required', array('required' => 'Satker'.$this->string_text['harus_diisi']));
	$this->form_validation->set_error_delimiters('<span class="text-danger"><b>', '</b></span>');
        if ($this->form_validation->run() == FALSE) {
            $this->edit($this->input->post($this->id, TRUE));
        } else {
            $data = array(
                $this->idt_ikpa_satker => $this->input->post($this->idt_ikpa_satker,TRUE),
                $this->idr_tahun => $this->input->post($this->idr_tahun,TRUE),
                $this->idr_bulan => $this->input->post($this->idr_bulan,TRUE),
                $this->idr_satker => $this->input->post($this->idr_satker,TRUE),
	    );
            $this->m_data->update_data($this->id.' = '.$idt_ikpa_satker, $data, $this->table);
            // Update T_user
            $id = $this->input->post($this->id,TRUE);
            // Mode Log Aktif
            if(getInfoAPP('flag_log') == 'ON') {
                // Isi Tabel Log User
                $this->m_data->insert_log('Edit Ikpa Satker '.$id.'-'.date('Y-m-d H:m:s'),$this->session->userdata('id'));
            }
            $this->session->set_flashdata('message', $this->string_text['ubah_data_berhasil']);
            redirect(site_url($this->controller));
        }
    }
    
    public function hapus($id) 
    {
        $row = $this->m_data->get_by_id($this->table, $this->id,$id);
        if ($row) {
            // Mode Log Aktif
            if(getInfoAPP('flag_log') == 'ON') {
                // Isi Tabel Log User
                $this->m_data->insert_log('Delete Ikpa Satker '.$id.'-'.date('Y-m-d H:m:s'),$this->session->userdata('id'));
            }
            $this->m_data->delete_data($this->id.' = '.$id,$this->table);
            $this->session->set_flashdata('message', $this->string_text['hapus_data_berhasil']);
            redirect(site_url($this->controller));
        } else {
            $this->session->set_flashdata('message', $this->string_text['data_tidak_ada']);
            redirect(site_url($this->controller));
        }
    }
    
    public function detail($id,$string_menu) 
    {
        // Mode Log Aktif
        if(getInfoAPP('flag_log') == 'ON') {
            // Isi Tabel Log User
            $this->m_data->insert_log('Menu Detail IKPA SATKER '.date('Y-m-d H:m:s'),$this->session->userdata('id'));
        }
        //$row = $this->m_data->get_by_id($this->view, $this->id,$id);
        $data = array(
            'judul' => 'DETAIL IKPA SATKER',
            'id' => $id,
            'string_menu' => $string_menu,
            'controller' => $this->controller,
            'controller_evaluasi' => 'c_evaluasi',
            'judul_evaluasi' => 'Evaluasi',
            'judul_final' => 'Finalkan Data',
            'tombol_kembali' => $this->tombol_kembali,
        );
        $this->template->load('template','ikpa_satker/v_ikpa_satker_detail',$data);
    }
    
    function cek_ikpa_satker() {
        $idr_tahun = $this->input->post('idr_tahun');
        $idr_bulan = $this->input->post('idr_bulan');
        $idr_satker = $this->input->post('idr_satker');
        $this->db->select('idt_ikpa_satker');
        $this->db->from('t_ikpa_satker');
        $this->db->where('idr_tahun', $idr_tahun);
        $this->db->where('idr_bulan', $idr_bulan);
        $this->db->where('idr_satker', $idr_satker);
        $query = $this->db->get();
        $num = $query->num_rows();
        if ($num > 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
    
    public function final($id,$string_menu) 
    {
        $row = $this->m_data->get_by_id($this->table, $this->id,$id);
        if($row->nilai_akhir_ikpa > 90) {
            $data = array(
                $this->flag_posting => 1,
                $this->tanggal_posting => date('Y-m-d H:i:s'),
            );
            $this->m_data->update_data($this->id.' = '.$id, $data, $this->table);
            if(getInfoAPP('flag_log') == 'ON') {
                // Isi Tabel Log User
                $this->m_data->insert_log('Finalkan Data IKPA Satker ID:'.$row->idt_ikpa_satker.' Pada '.date('Y-m-d H:m:s'),$this->session->userdata('id'));
            }
        }
        redirect(site_url($this->controller.'/detail/'.$id.'/'.$string_menu));
    }
    
    public function draft($id,$string_menu) 
    {
        $row = $this->m_data->get_by_id($this->table, $this->id,$id);
        $id_role = $this->session->userdata('id_role');
        if($id_role == 1 || $id_role == 2) {
            if($row->flag_posting == 1) {
                $data = array(
                    $this->flag_posting => 0,
                    $this->tanggal_posting => NULL,
                );
                $this->m_data->update_data($this->id.' = '.$id, $data, $this->table);
                if(getInfoAPP('flag_log') == 'ON') {
                    // Isi Tabel Log User
                    $this->m_data->insert_log('Draftkan Data IKPA Satker ID:'.$row->idt_ikpa_satker.' Pada '.date('Y-m-d H:m:s'),$this->session->userdata('id'));
                }
            }
        }
        redirect(site_url($this->controller.'/detail/'.$id.'/'.$string_menu));
    }
    
}
