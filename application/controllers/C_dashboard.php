<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of C_dashboard
 *
 * @author Dicky Satria Utama
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class C_dashboard extends CI_Controller 
{
    
    public $CI;
    public $string_text;
    public $errors = array();
    public $infos = array();
    
    // pertama masuk kesini
    public function __construct() {
        // Settingan Umum
        // Judul Tab
        parent::__construct();
        $this->load->model('m_data');
        $this->errors = array();
        $this->CI = & get_instance();
        $this->CI->load->library('session');
        $this->CI->load->library('email');
        $this->CI->load->helper('url');
        $this->CI->load->helper('string');
        $this->CI->load->helper('email');
        $this->CI->load->database();
        $this->CI->config->load('string_text');
        $this->string_text = $this->CI->config->item('text');
    }
    
    public function kosong($string_menu)
    {
        $id = $this->session->userdata('id');
        $data['string_menu'] = $string_menu;
        // Mode Log Aktif
        if(getInfoAPP('flag_log') == 'ON') {
            // Isi Tabel Log User
            $this->m_data->insert_log('Login '.date('Y-m-d H:m:s'),$id);
        }
        $this->template->load('template','dashboard/v_dashboard_kosong',$data);
    }
    
    
}
