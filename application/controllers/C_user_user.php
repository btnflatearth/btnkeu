<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of C_user
 *
 * @author Dicky Satria Utama
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class C_user_user extends CI_Controller
{
    public $CI;
    public $string_text;
    public $controller = 'C_user_user';
    public $controller_singkat = 'user';
    public $table = 't_user';
    public $view = 'zzz_v_user';
    public $id = 'idt_user';
    public $field = 'idt_user,email,nama,unit_kerja_peg,unit_kerja_es2,status,last_login,role_name,akses_unit,log';
    public $tombol_simpan = 'Simpan';
    public $tombol_kembali = 'Kembali';
    public $judul_list = 'List User';
    public $judul_form = 'Form User';
    public $judul_detail = 'Detail User';
    public $folder = 'user';
    // Semua Kolom yg ada di tabel T_user
    public $idt_user = 'idt_user';
    public $email = 'email';
    public $password = 'password';
    public $idr_pegawai = 'idr_pegawai';
    public $idr_role = 'idr_role';
    public $status = 'status';
    public $created_date = 'created_date';
    public $created_by = 'created_by';
    public $updated_date = 'updated_date';
    public $updated_by = 'updated_by';
    
    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('m_data');
        $this->load->library('form_validation');        
        $this->load->library('datatables');
        $this->CI = & get_instance();
        $this->CI->config->load('string_text');
        $this->string_text = $this->CI->config->item('text');
    }
    
    public function index()
    {
        // Mode Log Aktif
        if(getInfoAPP('flag_log') == 1) {
            // Isi Tabel Log User
            $this->m_data->insert_log('Menu List User '.date('Y-m-d H:m:s'),$this->session->userdata('idt_user'));
        }
        $data['judul_list'] = $this->judul_list;
        $data['controller'] = $this->controller;
        $data['field'] = $this->field;
        $data['table'] = $this->table;
        $data['id'] = $this->id;
        $data['flag_export_excel'] = FALSE;
        $data['flag_export_word'] = FALSE;
        $data['flag_detail'] = TRUE;
        $role_code = $this->session->userdata('role_code');
        if($role_code == 'ADM') {
            $data['flag_delete'] = TRUE;
            $data['flag_tambah'] = TRUE;
            $data['flag_edit'] = TRUE;
        } else {
            $data['flag_delete'] = FALSE;
            $data['flag_tambah'] = FALSE;
            $data['flag_edit'] = FALSE;
        }
        $this->template->load('template',$this->folder.'/v_user_user_list',$data);
    } 
    
    public function json() {
        header('Content-Type: application/json');
        $role_code = $this->session->userdata('role_code');
        if($role_code == 'SKRS') {
            $where_in = $this->session->userdata('akses');
            $this->datatables->where_in('id_unit_eselon2', $where_in);
        }
        echo $this->m_data->json($this->field,$this->view,$this->id,$this->controller);
    }
    
    public function tambah() 
    {
        // Mode Log Aktif
        if(getInfoAPP('flag_log') == 1) {
            // Isi Tabel Log User
            $this->m_data->insert_log('Form Tambah Data User '.date('Y-m-d H:m:s'),$this->session->userdata('idt_user'));
        }
        $data = array(
            'judul_form' => $this->judul_form,
            'tombol_simpan' => $this->tombol_simpan,
            'tombol_kembali' => $this->tombol_kembali,
            'controller' => $this->controller,
            'id' => $this->id,
            'aksi' => site_url($this->controller.'/aksi_tambah'),
	    'id_value' => set_value($this->id),
            $this->email => set_value($this->email),
            $this->idr_pegawai => set_value($this->idr_pegawai),
            $this->idr_role => set_value($this->idr_role,9),
	);
        $data['pegawailist'] = $this->m_data->get_data('zzz_v_pegawai')->result();
        $this->template->load('template',$this->folder.'/v_user_user_form', $data);
    }
    
    public function aksi_tambah() 
    {
        $this->form_validation->set_rules($this->email, 'Email', 'trim|is_unique[t_user.email]|required', array('is_unique' => 'Email'.$this->string_text['sudah_ada'], 'required' => 'Email'.$this->string_text['harus_diisi']));
        $this->form_validation->set_rules($this->idr_pegawai, 'Nama Pegawai', 'required|callback_cek_user_double', array('required' => 'Nama Pegawai '.$this->string_text['harus_diisi'], 'cek_user_double' => 'Data User '.$this->string_text['sudah_ada']));
	$this->form_validation->set_error_delimiters('<span class="text-danger"><b>', '</b></span>');
        if ($this->form_validation->run() == FALSE) {
            $this->tambah();
        } else {
            $data = array(
                $this->email => $this->input->post($this->email,TRUE),
                $this->idr_pegawai => $this->input->post($this->idr_pegawai,TRUE),
                $this->password => md5(getInfoAPP('default_password')),
                $this->idr_role => $this->input->post($this->idr_role,TRUE),
                $this->created_by => strtoupper($this->session->userdata($this->email)),
	    );
            $this->m_data->insert_data($data, $this->table);
            $id = $this->db->insert_id();
            // Mode Log Aktif
            if(getInfoAPP('flag_log') == 1) {
                // Isi Tabel Log User
                $this->m_data->insert_log('Tambah Data User '.$id.'-'.date('Y-m-d H:m:s'),$this->session->userdata('idt_user'));
            }
            $this->session->set_flashdata('message', $this->string_text['simpan_data_berhasil']);
            redirect(site_url($this->controller));
        }
    }
    
    public function edit($id) 
    {
        // Mode Log Aktif
        if(getInfoAPP('flag_log') == 1) {
            // Isi Tabel Log User
            $this->m_data->insert_log('Form Edit User '.date('Y-m-d H:m:s'),$this->session->userdata('idt_user'));
        }
        $row = $this->m_data->get_by_id($this->table, $this->id,$id);
        if ($row) {
            $data = array(
                'judul_form' => $this->judul_form,
                'tombol_simpan' => $this->tombol_simpan,
                'tombol_kembali' => $this->tombol_kembali,
                'controller' => $this->controller,
                'id' => $this->id,
                'aksi' => site_url($this->controller.'/aksi_edit'),
                'id_value' => set_value('id_value', $row->idt_user),
                $this->email => set_value($this->email, $row->email),
		$this->idr_pegawai => set_value($this->idr_pegawai, $row->idr_pegawai)
	    );
            $data['pegawailist'] = $this->m_data->get_data('zzz_v_pegawai')->result();
            $this->template->load('template',$this->folder.'/v_user_user_form', $data);
        } else {
            $this->session->set_flashdata('message', $this->string_text['data_tidak_ada']);
            redirect(site_url($this->controller));
        }
    }
    
    public function aksi_edit() 
    {
        $idt_user = $this->input->post($this->id, TRUE);
        $idr_pegawai = $this->input->post($this->idr_pegawai, TRUE);
        $idr_role = $this->input->post($this->idr_role, TRUE);
        $row = $this->m_data->get_by_id($this->table, $this->id,$idt_user);
        if($row->email != $this->input->post($this->email)) {
            $this->form_validation->set_rules($this->user_email, 'Email', 'trim|is_unique[t_user.email]|required', array('is_unique' => 'Email'.$this->string_text['sudah_ada'], 'required' => 'Email'.$this->string_text['harus_diisi']));
        } else {
            $this->form_validation->set_rules($this->user_email, 'Email', 'trim|required', array('required' => 'Email'.$this->string_text['harus_diisi']));
        }
        if(($row->idr_pegawai.'-'.$row->idr_role) != ($idr_pegawai.'-'.$idr_role)) {
            $this->form_validation->set_rules($this->idr_pegawai, 'Nama Pegawai', 'required|callback_cek_user_double', array('required' => 'Nama Pegawai '.$this->string_text['harus_diisi'], 'cek_user_double' => 'Data User '.$this->string_text['sudah_ada']));
        } else {
            $this->form_validation->set_rules($this->idr_pegawai, 'Nama Pegawai', 'required', array('required' => 'Nama Pegawai '.$this->string_text['harus_diisi']));
        }
	$this->form_validation->set_error_delimiters('<span class="text-danger"><b>', '</b></span>');
        if ($this->form_validation->run() == FALSE) {
            $this->edit($this->input->post($this->id, TRUE));
        } else {
            $data = array(
                $this->idt_user => $this->input->post($this->id,TRUE),
                $this->email => $this->input->post($this->email,TRUE),
                $this->idr_pegawai => $this->input->post($this->idr_pegawai,TRUE),
                $this->updated_by => strtoupper($this->session->userdata($this->email)),
	    );
            $this->m_data->update_data($this->id.' = '.$idt_user, $data, $this->table);
            // Update T_user
            $id = $this->input->post($this->id,TRUE);
            // Mode Log Aktif
            if(getInfoAPP('flag_log') == 1) {
                // Isi Tabel Log User
                $this->m_data->insert_log('Edit User '.$id.'-'.date('Y-m-d H:m:s'),$this->session->userdata('idt_user'));
            }
            $this->session->set_flashdata('message', $this->string_text['ubah_data_berhasil']);
            redirect(site_url($this->controller));
        }
    }
    
    public function hapus($id) 
    {
        $row = $this->m_data->get_by_id($this->table, $this->id,$id);
        if ($row) {
            // Mode Log Aktif
            if(getInfoAPP('flag_log') == 1) {
                // Isi Tabel Log User
                $this->m_data->insert_log('Delete User '.$id.'-'.date('Y-m-d H:m:s'),$this->session->userdata('idt_user'));
            }
            $this->m_data->delete_data($this->idt_user.' = '.$id,$this->table);
            $this->session->set_flashdata('message', $this->string_text['hapus_data_berhasil']);
            redirect(site_url($this->controller));
        } else {
            $this->session->set_flashdata('message', $this->string_text['data_tidak_ada']);
            redirect(site_url($this->controller));
        }
    }
    
    function cek_user_double() {
        $idr_pegawai = $this->input->post('idr_pegawai');
        $idr_role = $this->input->post('idr_role');
        $this->db->select('idt_user');
        $this->db->from('t_user');
        //$this->db->like('email', $email);
        $this->db->where('idr_pegawai', $idr_pegawai);
        $this->db->where('idr_role', $idr_role);
        $query = $this->db->get();
        $num = $query->num_rows();
        if ($num > 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
    
    public function detail($id) 
    {
        // Mode Log Aktif
        if(getInfoAPP('flag_log') == 1) {
            // Isi Tabel Log User
            $this->m_data->insert_log('Menu Detail User '.date('Y-m-d H:m:s'),$this->session->userdata('idt_user'));
        }
        //$row = $this->m_data->get_by_id($this->view, $this->id,$id);
        $data = array(
            'judul' => 'Detail User',
            'idt_user' => $id,
            'controller' => $this->controller,
            'tombol_kembali' => $this->tombol_kembali,
            'judul_akses_unit' => 'Hak Akses Unit',
            'judul_log' => 'Log Kegiatan User'
        );
        $this->template->load('template','user/v_user_user_detail',$data);
    }
    
    public function edit_role($id) 
    {
        // Mode Log Aktif
        if(getInfoAPP('flag_log') == 1) {
            // Isi Tabel Log User
            $this->m_data->insert_log('Form Edit Role '.date('Y-m-d H:m:s'),$this->session->userdata('idt_user'));
        }
        $row = $this->m_data->get_by_id($this->table, $this->id,$id);
        if ($row) {
            $data = array(
                'judul_form' => 'Form Edit Role',
                'tombol_simpan' => $this->tombol_simpan,
                'tombol_kembali' => $this->tombol_kembali,
                'controller' => $this->controller,
                'id' => $this->id,
                'aksi' => site_url($this->controller.'/aksi_edit_role'),
                'id_value' => set_value('id_value', $row->idt_user),
                $this->idr_role => set_value($this->idr_role, $row->idr_role)
	    );
            $this->db->where_not_in('idr_role', array(1));
            $data['rolelist'] = $this->m_data->get_data('r_role')->result();
            $this->template->load('template',$this->folder.'/v_user_role_form', $data);
        } else {
            $this->session->set_flashdata('message', $this->string_text['data_tidak_ada']);
            redirect(site_url($this->controller.'/detail/'.$id));
        }
    }
    
    public function aksi_edit_role() 
    {
        $idt_user = $this->input->post($this->id, TRUE);
        $this->form_validation->set_rules($this->idr_role, 'Role', 'required', array('required' => 'Role '.$this->string_text['harus_diisi']));
	$this->form_validation->set_error_delimiters('<span class="text-danger"><b>', '</b></span>');
        if ($this->form_validation->run() == FALSE) {
            $this->edit($this->input->post($this->id, TRUE));
        } else {
            $data = array(
                $this->idt_user => $this->input->post($this->id,TRUE),
                $this->idr_role => $this->input->post($this->idr_role,TRUE),
                $this->updated_by => strtoupper($this->session->userdata($this->email)),
	    );
            $this->m_data->update_data($this->id.' = '.$idt_user, $data, $this->table);
            // Update T_user
            $id = $this->input->post($this->id,TRUE);
            // Mode Log Aktif
            if(getInfoAPP('flag_log') == 1) {
                // Isi Tabel Log User
                $this->m_data->insert_log('Edit User Role '.$id.'-'.date('Y-m-d H:m:s'),$this->session->userdata('idt_user'));
            }
            $this->session->set_flashdata('message', $this->string_text['ubah_data_berhasil']);
            redirect(site_url($this->controller.'/detail/'.$id));
        }
    }
    
    public function user_off($id) 
    {
        $data = array(
            $this->status => 0,
            $this->updated_by => strtoupper($this->session->userdata($this->email)),
        );
        $this->m_data->update_data($this->id.' = '.$id, $data, $this->table);
        redirect(site_url($this->controller.'/detail/'.$id));
    }
    
    public function user_on($id) 
    {
        $data = array(
            $this->status => 1,
            $this->updated_by => strtoupper($this->session->userdata($this->email)),
        );
        $this->m_data->update_data($this->id.' = '.$id, $data, $this->table);
        redirect(site_url($this->controller.'/detail/'.$id));
    }
    
    public function json_akses_unit($idt_user) {
        header('Content-Type: application/json');
        $field = 'idt_user_akses_unit,idr_user,unit_kerja';
        $view = 'zzz_v_user_akses_unit';
        $id = 'idt_user_akses_unit';
        $controller = $this->controller;
        $where = 'idr_user = '.$idt_user;
        echo $this->m_data->json_where_var($field,$view,$id,$controller,$where,'_akses_unit');
    }
    
    public function tambah_akses_unit($idt_user) 
    {
        // Mode Log Aktif
        if(getInfoAPP('flag_log') == 1) {
            // Isi Tabel Log User
            $this->m_data->insert_log('Form Tambah Data User Akses '.date('Y-m-d H:m:s'),$this->session->userdata('idt_user'));
        }
        $data = array(
            'judul_form' => 'Form Tambah User Akses',
            'tombol_simpan' => $this->tombol_simpan,
            'tombol_kembali' => $this->tombol_kembali,
            'controller' => $this->controller,
            'id' => 'idt_user_akses_unit',
            'aksi' => site_url($this->controller.'/aksi_tambah_akses_unit'),
	    'id_value' => set_value('idt_user_akses_unit'),
            'idr_user' => set_value('idr_user',$idt_user),
            'idr_unit' => set_value('idr_unit'),
	);
        $role_code = $this->session->userdata('role_code');
        if($role_code == 'SKRS') {
            $where_in = $this->session->userdata('akses');
            $this->datatables->where_in('id_unit_eselon2', $where_in);
        }
        $data['unitlist'] = $this->m_data->get_data('r_unit')->result();
        $this->template->load('template',$this->folder.'/v_user_akses_unit_form', $data);
    }
    
    public function aksi_tambah_akses_unit() 
    {
        $idr_user = $this->input->post('idr_user',TRUE);
        $this->form_validation->set_rules('idr_unit', 'Nama Unit', 'required|callback_cek_user_akses_double', array('required' => 'Nama Unit '.$this->string_text['harus_diisi'], 'cek_user_akses_double' => 'Data User Akses '.$this->string_text['sudah_ada']));
	$this->form_validation->set_error_delimiters('<span class="text-danger"><b>', '</b></span>');
        if ($this->form_validation->run() == FALSE) {
            $this->tambah_akses_unit($idr_user);
        } else {
            $data = array(
                'idr_user' => $this->input->post('idr_user',TRUE),
                'idr_unit' => $this->input->post('idr_unit',TRUE),
                $this->created_by => strtoupper($this->session->userdata($this->email)),
	    );
            $this->m_data->insert_data($data, 't_user_akses_unit');
            $id = $this->db->insert_id();
            // Mode Log Aktif
            if(getInfoAPP('flag_log') == 1) {
                // Isi Tabel Log User
                $this->m_data->insert_log('Tambah Data User Akses '.$id.'-'.date('Y-m-d H:m:s'),$this->session->userdata('idt_user'));
            }
            $this->session->set_flashdata('message', $this->string_text['simpan_data_berhasil']);
            redirect(site_url($this->controller.'/detail/'.$idr_user));
        }
    }
    
    public function edit_akses_unit($id) 
    {
        // Mode Log Aktif
        if(getInfoAPP('flag_log') == 1) {
            // Isi Tabel Log User
            $this->m_data->insert_log('Form Edit User Akses '.date('Y-m-d H:m:s'),$this->session->userdata('idt_user'));
        }
        $row = $this->m_data->get_by_id('t_user_akses_unit', 'idt_user_akses_unit',$id);
        if ($row) {
            $data = array(
                'judul_form' => $this->judul_form,
                'tombol_simpan' => $this->tombol_simpan,
                'tombol_kembali' => $this->tombol_kembali,
                'controller' => $this->controller,
                'id' => 'idt_user_akses_unit',
                'aksi' => site_url($this->controller.'/aksi_edit_akses_unit'),
                'id_value' => set_value('id_value', $row->idt_user_akses_unit),
                'idr_user' => set_value('idr_user', $row->idr_user),
		'idr_unit' => set_value('idr_unit', $row->idr_unit)
	    );
            $role_code = $this->session->userdata('role_code');
            if($role_code == 'SKRS') {
                $where_in = $this->session->userdata('akses');
                $this->datatables->where_in('id_unit_eselon2', $where_in);
            }
            $data['unitlist'] = $this->m_data->get_data('r_unit')->result();
            $this->template->load('template',$this->folder.'/v_user_akses_unit_form', $data);
        } else {
            $this->session->set_flashdata('message', $this->string_text['data_tidak_ada']);
            redirect(site_url($this->controller.'/detail/'.$row->idr_user));
        }
    }
    
    public function aksi_edit_akses_unit() 
    {
        $idt_user_akses_unit = $this->input->post('idt_user_akses_unit', TRUE);
        $row = $this->m_data->get_by_id('t_user_akses_unit', 'idt_user_akses_unit',$idt_user_akses_unit);
        if(($row->idr_user.$row->idr_unit) != ($this->input->post('idr_user').$this->input->post('idr_unit'))) {
            $this->form_validation->set_rules('idr_unit', 'Nama Unit', 'required|callback_cek_user_akses_double', array('required' => 'Nama Unit '.$this->string_text['harus_diisi'], 'cek_user_akses_double' => 'Data User Akses '.$this->string_text['sudah_ada']));
        } else {
            $this->form_validation->set_rules('idr_unit', 'Nama Unit', 'required', array('required' => 'Nama Unit '.$this->string_text['harus_diisi']));
        }
        $this->form_validation->set_error_delimiters('<span class="text-danger"><b>', '</b></span>');
        if ($this->form_validation->run() == FALSE) {
            $this->edit_akses_unit($this->input->post('idt_user_akses_unit', TRUE));
        } else {
            $data = array(
                'idt_user_akses_unit' => $this->input->post('idt_user_akses_unit',TRUE),
                'idr_user' => $this->input->post('idr_user',TRUE),
                'idr_unit' => $this->input->post('idr_unit',TRUE),
                $this->updated_by => strtoupper($this->session->userdata($this->email)),
	    );
            $this->m_data->update_data('idt_user_akses_unit'.' = '.$idt_user_akses_unit, $data, 't_user_akses_unit');
            // Update T_user
            $id = $this->input->post('idt_user_akses_unit',TRUE);
            // Mode Log Aktif
            if(getInfoAPP('flag_log') == 1) {
                // Isi Tabel Log User
                $this->m_data->insert_log('Edit User Akses'.$id.'-'.date('Y-m-d H:m:s'),$this->session->userdata('idt_user'));
            }
            $this->session->set_flashdata('message', $this->string_text['ubah_data_berhasil']);
            redirect(site_url($this->controller.'/detail/'.$this->input->post('idr_user',TRUE)));
        }
    }
    
    public function hapus_akses_unit($id) 
    {
        $row = $this->m_data->get_by_id('t_user_akses_unit', 'idt_user_akses_unit',$id);
        if ($row) {
            // Mode Log Aktif
            if(getInfoAPP('flag_log') == 1) {
                // Isi Tabel Log User
                $this->m_data->insert_log('Delete User Akses '.$id.'-'.date('Y-m-d H:m:s'),$this->session->userdata('idt_user'));
            }
            $this->m_data->delete_data('idt_user_akses_unit'.' = '.$id,'t_user_akses_unit');
            $this->session->set_flashdata('message', $this->string_text['hapus_data_berhasil']);
            redirect(site_url($this->controller.'/detail/'.$row->idr_user));
        } else {
            $this->session->set_flashdata('message', $this->string_text['data_tidak_ada']);
            redirect(site_url($this->controller.'/detail/'.$row->idr_user));
        }
    }
    
    function cek_user_akses_double() {
        $idr_user = $this->input->post('idr_user');
        $idr_unit = $this->input->post('idr_unit');
        $this->db->select('idt_user_akses_unit');
        $this->db->from('t_user_akses_unit');
        //$this->db->like('email', $email);
        $this->db->where('idr_user', $idr_user);
        $this->db->where('idr_unit', $idr_unit);
        $query = $this->db->get();
        $num = $query->num_rows();
        if ($num > 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
    
    public function json_log($idt_user) {
        header('Content-Type: application/json');
        $field = 'idt_log_user,idt_user,email,log_user,created_date';
        $view = 'zzz_v_log_user';
        $id = 'idt_log_user';
        $controller = $this->controller;
        $where = 'idt_user = '.$idt_user;
        echo $this->m_data->json_where($field,$view,$id,$controller,$where);
    }
    
}
