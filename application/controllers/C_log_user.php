<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of C_user
 *
 * @author Dicky Satria Utama
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class C_log_user extends CI_Controller
{
    public $CI;
    public $string_text;
    public $controller = 'C_log_user';
    public $controller_singkat = 'log_user';
    public $table = 't_log_user';
    public $view = 't_log_user';
    public $id = 'idt_log_user';
    public $field = 'idt_log_user,id,name,log_user,created_date';
    public $tombol_simpan = 'Simpan';
    public $tombol_kembali = 'Kembali';
    public $judul_list = 'List Log User';
    public $judul_form = 'Form Log User';
    public $judul_detail = 'Detail Log User';
    public $folder = 'log_user';
    
    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('m_data');
        $this->load->library('form_validation');        
        $this->load->library('datatables');
        $this->CI = & get_instance();
        $this->CI->config->load('string_text');
        $this->string_text = $this->CI->config->item('text');
    }
    
    public function index()
    {
        // Mode Log Aktif
        if(getInfoAPP('flag_log') == 'ON') {
            // Isi Tabel Log User
            $this->m_data->insert_log('Menu List Log User '.date('Y-m-d H:m:s'),$this->session->userdata('id'));
        }
        $data['judul_list'] = $this->judul_list;
        $data['controller'] = $this->controller;
        $data['field'] = $this->field;
        $data['table'] = $this->table;
        $data['id'] = $this->id;
        $data['flag_tambah'] = FALSE;
        $data['flag_export_excel'] = FALSE;
        $data['flag_export_word'] = FALSE;
        $data['flag_edit'] = FALSE;
        $data['flag_detail'] = FALSE;
        $data['flag_delete'] = FALSE;
        $this->template->load('template',$this->folder.'/v_log_user_list',$data);
    } 
    
    public function json() {
        header('Content-Type: application/json');
        //$this->db->order_by('created_date', 'DESC');
        echo $this->m_data->json($this->field,$this->view,$this->id,$this->controller);
    }
    
}
