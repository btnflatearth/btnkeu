<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of C_auth
 *
 * @author Dicky Satria Utama
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class C_auth extends CI_Controller 
{
    public $CI;
    public $string_text;
    public $errors = array();
    public $infos = array();
    
    // pertama masuk kesini
    public function __construct() {
        // Settingan Umum
        // Judul Tab
        parent::__construct();
        $this->load->model('m_data');
        $this->errors = array();
        $this->CI = & get_instance();
        $this->CI->load->library('session');
        //$this->CI->load->library('name');
        $this->CI->load->helper('url');
        $this->CI->load->helper('string');
        //$this->CI->load->helper('name');
        $this->CI->load->database();
        $this->CI->config->load('string_text');
        $this->string_text = $this->CI->config->item('text');
    }
    
    function index() 
    {
        $this->load->view('v_login');
    }
    
    function login(){
        $name = $this->input->post('name');
        $pass = $this->input->post('pass');
        if($this->cek_login($name, $pass)){
            redirect('c_dashboard/kosong/ksg');
        } else {
            $this->session->set_flashdata('status_login','2:debug');
            redirect('auth');
        }
    }
    
    function cek_login($name,$pass){
        $this->db->where('name',$name);
        $this->db->where('pass',md5($pass));
        $user = $this->db->get('aauth_users');
        
//        if(!ctype_alnum($pass) or strlen($pass) < getInfoAPP('min_pass') or strlen($pass) > getInfoAPP('max_pass')) {
//           $this->session->set_flashdata('status_login',$this->string_text['user_not_in_criteria']);
//           return FALSE;
//        }
        
        if($user->num_rows()>0){
            $this->db->where('name',$name);
            $this->db->where('pass',md5($pass));
            $this->db->where('status','Aktif');
            $user_aktif = $this->db->get('v_users');
            if($user_aktif->num_rows()>0){
                //echo '1-----';
                $this->session->set_userdata($user_aktif->row_array());
                $id = $this->session->userdata('id');
                
                // Akses Unit
                $akses = $this->m_data->get_data_filter('v_t_user_satker',"id = $id")->result_array();
                $this->session->set_userdata(array('akses' => array_column($akses, 'idr_satker')));
                $this->session->set_userdata(array('akses_satker' => array_column($akses, 'nama_satker')));
                
                // Update waktu terakhir login untuk user
                $user = array(
                    //'pass' => md5($pass),
                    //'updated_by' => 'LDAP',,
                    'last_login' => date('Y-m-d H:m:s')
                );
                $this->db->where('id', $id);
                $this->db->update('aauth_users', $user);
                // Mode Log Aktif
                if(getInfoAPP('flag_log') == 'ON') {
                    // Isi Tabel Log User
                    $this->m_data->insert_log('Login '.date('Y-m-d H:m:s'),$id);
                }
                return TRUE;
            } else {
                //echo '2-----';
                $this->session->set_flashdata('status_login',$this->string_text['user_block']);
                return FALSE;
            }
        } else {
            //echo '3-----';
            $this->session->set_flashdata('status_login',$this->string_text['gagal_login']);
            return FALSE;
        }
    }
    
    function logout(){
        // Mode Log Aktif
        if(getInfoAPP('flag_log') == 'ON') {
            // Isi Tabel Log User
            $this->m_data->insert_log('Logout '.date('Y-m-d H:m:s'),$this->session->userdata('id'));
        }
        $this->session->sess_destroy();
        $this->session->set_flashdata('status_login',$this->string_text['berhasil_logout']);
        redirect('auth');
    }
}
