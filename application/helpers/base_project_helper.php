<?php

function getInfoAPP($field){
    $ci = get_instance();
    $ci->db->where('flag_aktif',1);
    $app = $ci->db->get('r_setting')->row_array();
    return $app[$field];
}

function getInfoUser($field,$idt_user){
    $ci = get_instance();
    $ci->db->where('idt_user',$idt_user);
    $user = $ci->db->get('t_user')->row_array();
    return $user[$field];
}

function is_login(){
    $ci = get_instance();
    if(empty($ci->session->userdata('idt_user'))){
        redirect('auth');
    }
}

/* fungsi untuk mendapatkan value dari sebuah tabel
 * $table nama tabel yang digunakan
 * $field nama field yang ingin ditampilkan
 * $key ingin ditampilkan berdasarkan field yang mana
 * $value = berdasrkan apa
 */
function getFieldValue($table,$field,$key,$value){
    $ci = get_instance();
    $ci->db->where($key,$value);
    $data = $ci->db->get($table)->row_array();
    return $data[$field];
}

function getRomawi($bln){
    switch ($bln){
    case 1: 
        return "I";
    break;
    case 2:
        return "II";
    break;
    case 3:
        return "III";
    break;
    case 4:
        return "IV";
    break;
    case 5:
        return "V";
    break;
    case 6:
        return "VI";
    break;
    case 7:
        return "VII";
    break;
    case 8:
        return "VIII";
    break;
    case 9:
        return "IX";
    break;
    case 10:
        return "X";
    break;
    case 11:
        return "XI";
    break;
    case 12:
        return "XII";
    break;
    }
}

?>
