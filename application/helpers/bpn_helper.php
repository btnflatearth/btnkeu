<?php

function getInfoAPP($field){
    $ci = get_instance();
    $ci->db->where('flag_aktif','ON');
    $app = $ci->db->get('r_setting')->row_array();
    return $app[$field];
}

function getInfoUser($field,$id){
    $ci = get_instance();
    $ci->db->where('id',$id);
    $user = $ci->db->get('aauth_users')->row_array();
    return $user[$field];
}

function is_login(){
    $ci = get_instance();
    if(empty($ci->session->userdata('id'))){
        redirect('auth');
    }
}

function tgl_indo($tgl){
    $tanggal = substr($tgl,8,2);
    $bulan = getBulan(substr($tgl,5,2));
    $tahun = substr($tgl,0,4);
    return $tanggal.' '.$bulan.' '.$tahun;       
} 

function getBulan($bln){
    switch ($bln){
        case 1: 
            return "Januari";
            break;
        case 2:
            return "Febuari";
            break;
        case 3:
            return "Maret";
            break;
        case 4:
            return "April";
            break;
        case 5:
            return "Mei";
            break;
        case 6:
            return "Juni";
            break;
        case 7:
            return "Juli";
            break;
        case 8:
            return "Agustus";
            break;
        case 9:
            return "September";
            break;
        case 10:
            return "Oktober";
            break;
        case 11:
            return "November";
            break;
        case 12:
            return "Desember";
            break;
    }
} 

function hari_ini($w){
    $seminggu = array("Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu");
    $hari_ini = $seminggu[$w];
    return $hari_ini;
}

?>
