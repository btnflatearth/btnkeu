<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

$config['text'] = array(
    'gagal_login' => 'Email atau Password yang anda input salah',
    'berhasil_logout' => 'Anda sudah berhasil keluar dari aplikasi',
    'user_block' => 'User anda tidak aktif !!!',
    'login' => 'Silahkan Login terlebih dahulu',
    'no_data' => 'Tidak ada data untuk ditampilkan.',
    'ambil_20_data' => 'Diambil 20 Aktifitas Terakhir.',
    'harus_diisi' => ' Harus Diisi !!!',
    'sudah_ada' => ' Sudah Dipakai !!!',
    'simpan_data_berhasil' => 'Simpan Data Berhasil !!!',
    'ubah_data_berhasil' => 'Perubahan Data Berhasil !!!',
    'hapus_data_berhasil' => 'Hapus Data Berhasil !!!',
    'data_tidak_ada' => 'Data Tidak Ditemukan !!!',
    'default_password' => 'password',
    'info_menu_child' => 'Silahkan Pilih Menu Parent untuk Menambahkan Menu Child !!!',
    'user_not_in_criteria' => 'Jumlah Minimal dan Maksimal Karakter Password tidak sesuai Kriteria !!!',
    'format_email_not_valid' => 'Format Email yang dimasukan tidak valid !!!',
    'batas_coba_login' => 'Batas Percobaan Login User anda sudah habis !!!',
    'user_belum_terdaftar' => 'Data Pegawai Anda Belum Terdaftar di Aplikasi SADEWA Silahkan Hubungi Admin Unit atau Admin Aplikasi SADEWA !!!',
    'data_user_belum_lengkap' => 'Data User Belum Lengkap di Aplikasi SADEWA Silahkan Hubungi Admin Unit atau Admin Aplikasi SADEWA !!!',
);