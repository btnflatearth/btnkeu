<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?= $judul ?> 
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
		<div class="box box-success">
                    <div style="padding-bottom: 10px;"'>
                        <br>&nbsp;
                        <?php 
                            echo anchor(site_url('c_profile/ganti_password/').$id, '<i class="fa fa-pencil" aria-hidden="true"></i> Ubah Password', 'class="btn btn-danger btn-sm"'); 
                        ?>
                        <br>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <tr>
                                <td style="width: 20%"><strong>Nama</strong></td>
                                <td style="width: 1%"><strong>:</strong></td>
                                <td style="width: 60%"><?= $name ?></td>
                                <td style="width: 19%;text-align: center" rowspan="6">
                                    <img src="<?= base_url() ?>/assets/img/blank.png" style="width: 300px"><br><br>
                                    <?php 
                                        echo anchor(site_url('c_profile'), '<i class="fa fa-pencil" aria-hidden="true"></i> Upload Foto', 'class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Coming Soon !!\')"'); 
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 20%"><strong>Email</strong></td>
                                <td style="width: 1%"><strong>:</strong></td>
                                <td style="width: 79%"><?= $email ?></td>
                            </tr>
                            <tr>
                                <td style="width: 20%"><strong>Role</strong></td>
                                <td style="width: 1%"><strong>:</strong></td>
                                <td style="width: 79%"><?= $grup ?></td>
                            </tr>
                            <tr>
                                <td style="width: 20%"><strong>Status</strong></td>
                                <td style="width: 1%"><strong>:</strong></td>
                                <td style="width: 79%"><?= $status ?></td>
                            </tr>
                            <tr>
                                <td style="width: 20%"><strong>Akses Satker</strong></td>
                                <td style="width: 1%"><strong>:</strong></td>
                                <td style="width: 79%"><?= $akses_satker ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>