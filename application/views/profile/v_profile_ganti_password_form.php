<div class="content-wrapper">
    <section class="content">
        <div class="box box-success box-solid">
            <div class="box-header with-border">
                <h3 class="box-title"><b><?= strtoupper($judul) ?></b></h3>
            </div>
            <form action="<?= $aksi ?>" method="post" enctype="multipart/form-data">
                <table class="table table-bordered">
                    <tr>
                        <td style="width: 200px"><b>Password Lama</b></td>
                        <td style="width: 10px"><b>:</b></td>
                        <td>
                            <input type="text" style="width: 300px" class="form-control" name="password_lama" id="password_lama" placeholder="Password Lama" value=""/>
                            <?php echo form_error('password_lama') ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 200px"><b>Password Baru</b></td>
                        <td style="width: 10px"><b>:</b></td>
                        <td>
                            <input type="text" style="width: 300px" class="form-control" name="password_baru" id="password_baru" placeholder="Password Baru" value=""/>
                            <?php echo form_error('password_baru') ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 200px"><b>Verifikasi Password Baru</b></td>
                        <td style="width: 10px"><b>:</b></td>
                        <td>
                            <input type="text" style="width: 300px" class="form-control" name="verifikasi_password_baru" id="verifikasi_password_baru" placeholder="Verifikasi Password Baru" value=""/>
                            <?php echo form_error('verifikasi_password_baru') ?>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
                            <button type="submit" class="btn btn-danger"><i class="fa fa-floppy-o"></i>
                                <?php echo $tombol ?>
                            </button> 
                            <a href="<?php echo site_url('profile') ?>" class="btn btn-info"><i class="fa fa-sign-out"></i> Kembali</a>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </section>
</div>