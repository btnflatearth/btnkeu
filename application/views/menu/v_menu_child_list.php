<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-success box-solid">
                    <div class="box-header">
                        <h3 class="box-title"><b><?= strtoupper($judul_list) ?></b></h3>
                    </div>
                    <div class="box-body">
                        <div style="padding-bottom: 10px;">
                            <a href="<?php echo site_url($controller_singkat) ?>" class="btn btn-info"><i class="fa fa-home"></i> <?= $tombol_home ?></a>
                            <?php 
                            foreach ($menuparentlist as $value) {
                            ?>
                                <a href="<?php echo site_url($controller.'/'.$function.'/'.$value['idr_menu']) ?>" class="btn btn-<?php if($id == $value['idr_menu']){echo'danger';}else{echo'info';} ?>"><i class="fa fa-arrow-right"></i> <?= $value['menu_name'] ?></a>
                            <?php
                            }
                            ?>
                        </div>
                        <div style="padding-bottom: 10px;">
                            <?php 
                                if($flag_tambah == TRUE) {
                                    echo anchor(site_url($controller.'/tambah/'.$id), '<i class="fa fa-wpforms" aria-hidden="true"></i> Tambah Data', 'class="btn btn-dropbox btn-sm"').' ';
                                }
                                if($flag_export_excel == TRUE) {
                                    echo anchor(site_url($controller.'/excel/'.$id), '<i class="fa fa-file-excel-o" aria-hidden="true"></i> Export Ms Excel', 'class="btn btn-success btn-sm"').' ';
                                }
                                if($flag_export_word == TRUE) {
                                    echo anchor(site_url($controller.'/word/'.$id), '<i class="fa fa-file-word-o" aria-hidden="true"></i> Export Ms Word', 'class="btn btn-flickr btn-sm"').' '; 
                                }
                            ?>
                        </div>
                        <table class="table table-bordered table-striped" id="mytable">
                            <thead>
                                <tr>
                                    <th style="width:20px;text-align: center">No</th>
                                    <th style="width:50px;text-align: center">Order</th>
                                    <th style="text-align: center">Nama Menu</th>
                                    <th style="text-align: center">Kode Menu</th>
                                    <th style="text-align: center">Controller Menu</th>
                                    <th style="text-align: center">Akses Menu</th>
                                    <th style="width:70px;text-align: center">Icon</th>
                                    <th style="width:50px;text-align: center">Status</th>
                                    <th style="width:50px;text-align: center">Offline</th>
                                    <th style="width:80px;text-align: center">UP/DOWN</th>
                                    <?php if($flag_edit == TRUE) { ?>
                                    <th style="width:40px;text-align: center">Edit</th>
                                    <?php } if($flag_detail == TRUE) { ?>
                                    <th style="width:40px;text-align: center">Detail</th>
                                    <?php } if($flag_delete == TRUE) { ?>
                                    <th style="width:40px;text-align: center">Delete</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th style="width:20px;text-align: center">No</th>
                                    <th style="width:50px;text-align: center">Order</th>
                                    <th style="text-align: center">Nama Menu</th>
                                    <th style="text-align: center">Kode Menu</th>
                                    <th style="text-align: center">Controller Menu</th>
                                    <th style="text-align: center">Akses Menu</th>
                                    <th style="width:70px;text-align: center">Icon</th>
                                    <th style="width:50px;text-align: center">Status</th>
                                    <th style="width:50px;text-align: center">Offline</th>
                                    <th style="width:80px;text-align: center">UP/DOWN</th>
                                    <?php if($flag_edit == TRUE) { ?>
                                    <th style="width:40px;text-align: center">Edit</th>
                                    <?php } if($flag_detail == TRUE) { ?>
                                    <th style="width:40px;text-align: center">Detail</th>
                                    <?php } if($flag_delete == TRUE) { ?>
                                    <th style="width:40px;text-align: center">Delete</th>
                                    <?php } ?>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#mytable").dataTable({
            "scrollX": true,
            "scrollY": "250px",
            "scrollCollapse": true,
            initComplete: function() {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function(e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                    }
                });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            ajax: {"url": "../../<?= $controller ?>/json/<?= $id ?>", "type": "POST"},
            columns: [{
                                        "data": "idr_menu",
                                        "className" : "text-center"
					},{
					"data": "menu_order"
					},{
					"data": "menu_name"
                                        },{
					"data": "menu_code"
                                        },{
                                        "data": "menu_controller"
                                        },{
                                        "data": "menu_akses_name"
                                        },{
					"data": "menu_icon"
                                        },{
                                        "data": "menu_status",
                                        "orderable": false,
                                        "className" : "text-center"
					},{
                                        "data": "menu_status_offline",
                                        "orderable": false,
                                        "className" : "text-center"
					},{
                                            "data" : "action_up_down",
                                            "orderable": false,
                                            "className" : "text-center"
                                        },
                                        <?php if($flag_edit == TRUE) { ?>
                                        {
                                            "data" : "action_edit",
                                            "orderable": false,
                                            "className" : "text-center"
                                        },
                                        <?php } if($flag_detail == TRUE) { ?>
                                        {
                                            "data" : "action_detail",
                                            "orderable": false,
                                            "className" : "text-center"
                                        },
                                        <?php } if($flag_delete == TRUE) { ?>
                                        {
                                        "data" : "action_delete",
                                        "orderable": false,
                                        "className" : "text-center"
                                        }
                                        <?php } ?>
            
            ],
            order: [[1, 'asc']],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
        
    });
</script>