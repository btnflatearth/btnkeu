<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-success box-solid">
                    <div class="box-header">
                        <h3 class="box-title"><b><?= strtoupper($judul_list) ?></b></h3>
                    </div>
                    <div class="box-body">
                        <div style="padding-bottom: 10px;">
                            <a href="<?php echo site_url($controller_singkat) ?>" class="btn btn-danger"><i class="fa fa-home"></i> <?= $tombol_home ?></a>
                            <?php 
                            foreach ($menuparentlist as $value) {
                            ?>
                                <a href="<?php echo site_url($controller.'/'.$function.'/'.$value['idr_menu']) ?>" class="btn btn-info"><i class="fa fa-arrow-right"></i> <?= $value['menu_name'] ?></a>
                            <?php
                            }
                            ?>
                            <br><br><font color="red"><b><i>* <?= $info_menu_child ?></i></b></font>
                        </div>
                        <table class="table table-bordered table-striped" id="mytable">
                            <thead>
                                <tr>
                                    <td style="text-align: center"><strong>Struktur Menu</strong></td>
                                </tr>
                            </thead>
                            <tr>
                                <td>
                                    <ul type="1">
                                    <?php 
                                    foreach ($menuparentlist as $parent) {
                                    ?>
                                        <li>
                                            <b><?= $parent['menu_name'] ?></b>
                                            <ul type="a">
                                            <?php 
                                            $this->load->model('m_data');
                                            $where = "idr_parent = ".$parent['idr_menu'];
                                            $menuchildlist = $this->m_data->get_data_filter_menu($this->table,$where,$this->menu_order,'ASC')->result_array();
                                            foreach ($menuchildlist as $child) {
                                            ?>
                                                <li><?= $child['menu_name'] ?></li>
                                            <?php } ?>
                                            </ul>
                                        </li>
                                    <?php
                                    }
                                    ?>
                                    </ul>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
