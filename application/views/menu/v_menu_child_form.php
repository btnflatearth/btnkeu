<div class="content-wrapper">
    <section class="content">
        <div class="box box-success box-solid">
            <div class="box-header with-border">
                <h3 class="box-title"><b><?= strtoupper($judul_form) ?></b></h3>
            </div>
            <form action="<?= $aksi ?>" method="post" enctype="multipart/form-data">
                <table class="table table-bordered">
                    <tr>
                        <td style="text-align: right;vertical-align: middle;width: 200px"><strong>Nama Menu</strong></td>
                        <td style="text-align: center;vertical-align: middle;width: 30px"><strong>:</strong></td>
                        <td>
                            <input type="text" style="width: 400px" class="form-control" name="menu_name" placeholder="Nama Menu" value="<?= $menu_name ?>" />
                        </td>
                        <td>
                            <?php echo '<b>'.form_error('menu_name').'</b>'; ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;vertical-align: middle;width: 200px"><strong>Kode Menu</strong></td>
                        <td style="text-align: center;vertical-align: middle;width: 30px"><strong>:</strong></td>
                        <td>
                            <input type="text" style="width: 400px" class="form-control" name="menu_code" placeholder="Kode Menu" value="<?= $menu_code ?>" />
                        </td>
                        <td>
                            <?php echo '<b>'.form_error('menu_code').'</b>'; ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;vertical-align: middle;width: 200px"><strong>Controller Menu</strong></td>
                        <td style="text-align: center;vertical-align: middle;width: 30px"><strong>:</strong></td>
                        <td>
                            <input type="text" style="width: 400px" class="form-control" name="menu_controller" placeholder="Controller Menu" value="<?= $menu_controller ?>" />
                        </td>
                        <td>
                            <?php echo '<b>'.form_error('menu_controller').'</b>'; ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;vertical-align: middle;width: 200px"><strong>Icon Menu</strong></td>
                        <td style="text-align: center;vertical-align: middle;width: 30px"><strong>:</strong></td>
                        <td>
                            <input type="text" style="width: 400px" class="form-control" name="menu_icon" placeholder="Icon Menu" value="<?= $menu_icon ?>" />
                        </td>
                        <td>
                            <?php echo '<b>'.form_error('menu_icon').'</b>'; ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;vertical-align: middle;width: 200px"><strong>Status</strong></td>
                        <td style="text-align: center;vertical-align: middle;width: 30px"><strong>:</strong></td>
                        <td>
                            <div class="form-check">
                                <input type="radio" class="form-check-input" id="materialUnchecked" value="AKTIF" name="menu_status" <?php if($menu_status == 'AKTIF') { echo 'checked="true"';} ?>>
                                <label class="form-check-label" for="materialUnchecked"><font color="green">AKTIF</font></label>
                                &nbsp;&nbsp;
                                <input type="radio" class="form-check-input" id="materialUnchecked" value="NONAKTIF" name="menu_status" <?php if($menu_status == 'NONAKTIF') { echo 'checked="true"';} ?>>
                                <label class="form-check-label" for="materialUnchecked"><font color="red">NONAKTIF</font></label>
                            </div>
                        </td>
                        <td>
                            <?php echo '<b>'.form_error('menu_status').'</b>'; ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;vertical-align: middle;width: 200px"><strong>Offline</strong></td>
                        <td style="text-align: center;vertical-align: middle;width: 30px"><strong>:</strong></td>
                        <td>
                            <div class="form-check">
                                <input type="radio" class="form-check-input" id="materialUnchecked" value="AKTIF" name="menu_status_offline" <?php if($menu_status_offline == 'AKTIF') { echo 'checked="true"';} ?>>
                                <label class="form-check-label" for="materialUnchecked"><font color="green">AKTIF</font></label>
                                &nbsp;&nbsp;
                                <input type="radio" class="form-check-input" id="materialUnchecked" value="NONAKTIF" name="menu_status_offline" <?php if($menu_status_offline == 'NONAKTIF') { echo 'checked="true"';} ?>>
                                <label class="form-check-label" for="materialUnchecked"><font color="red">NONAKTIF</font></label>
                            </div>
                        </td>
                        <td>
                            <?php echo '<b>'.form_error('menu_status_offline').'</b>'; ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;vertical-align: middle;width: 200px"><strong>Akses</strong></td>
                        <td style="text-align: center;vertical-align: middle;width: 30px"><strong>:</strong></td>
                        <td>
                            <?= 'Menu akses sebelumnya : <b>'.$menu_akses_name.'</b><br>' ?>
                            <?php 
                            $i = 1;
                            foreach ($grouplist as $group) {
                            ?>
                            <input type="checkbox" class="form-check-input" id="materialUnchecked" value="<?= $group->id ?>" name="id<?= $i ?>">
                            <input type="hidden" value="<?= $group->name ?>" name="name<?= $i ?>">
                            <input type="hidden" value="<?= $group->title ?>" name="title<?= $i ?>">
                            <label class="form-check-label" for="materialUnchecked"><font><?= ' '.$group->name ?></font></label>
                            <?php
                            $i++;
                            }
                            ?>
                        </td>
                        <td>
                            <?php //echo '<b>'.form_error('id').'</b>'; ?>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <input type="hidden" name="group_jumlah" value="<?php echo $i-1; ?>" /> 
                            <input type="hidden" name="idr_parent" value="<?php echo $idr_parent; ?>" /> 
                            <input type="hidden" name="<?= $id ?>" value="<?php echo $id_value; ?>" /> 
                            <button type="submit" class="btn btn-danger"><i class="fa fa-floppy-o"></i>
                                <?= $tombol_simpan ?>
                            </button> 
                            <a href="<?php echo site_url($controller.'/'.$function.'/'.$idr_parent) ?>" class="btn btn-info"><i class="fa fa-sign-out"></i> <?= $tombol_kembali ?></a>
                        </td>
                    </tr>
                </table>
            </form>        
        </div>
    </section>
</div>