<?php 
$row = $this->m_data->get_by_id('t_ikpa_satker', 'idt_ikpa_satker',$id);
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-success box-solid">
                <div class="box-header">
                    <h3 class="box-title"><b><?= strtoupper($judul_evaluasi) ?></b></h3>
                </div>
                <div class="box-body">
                    <div style="padding-bottom: 10px;">
                        <?php 
                            $id_role = $this->session->userdata('id_role');
                            if($id_role == 1 || $id_role == 2) {
                                if($row->flag_posting == 0) {
                                    echo anchor(site_url($controller_evaluasi.'/tambah/'), '<i class="fa fa-wpforms" aria-hidden="true"></i> Tambah Data', 'class="btn btn-dropbox btn-sm"').' ';
                                }
                            }
                        ?>
                    </div>
                    <table class="table table-bordered table-striped" id="mytable">
                        <thead>
                            <tr>
                                <th style="width:20px;text-align: center">No</th>
                                <?php
                                    if($row->flag_posting == 0) {
                                        echo '<th style="width:40px;text-align: center">Edit</th>';
                                        if($id_role == 1 || $id_role == 2) {
                                            echo '<th style="width:40px;text-align: center">Delete</th>';
                                        }
                                    } else {
                                        
                                    }
                                ?>
                                <th style="width:100px;text-align: center">Indikator</th>
                                <th style="width:100px;text-align: center">Penyebab</th>
                                <th style="width:300px;text-align: center">Uraian</th>
                                <th style="width:100px;text-align: center">Solusi</th>
                                <th style="width:300px;text-align: center">Uraian</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#mytable").dataTable({
            "scrollX": true,
            "scrollY": "300px",
            "scrollCollapse": true,
            initComplete: function() {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function(e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                    }
                });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            ajax: {"url": "<?= base_url().'/'.$controller_evaluasi ?>/json/<?= $id ?>", "type": "POST"},
            columns: [{
                                        "data": "idt_evaluasi",
                                        "className" : "text-center"
                                        },
                                        <?php
                                            if($row->flag_posting == 0) {
                                                ?>
                                                {
                                                    "data" : "action_edit",
                                                    "orderable": false,
                                                    "className" : "text-center"
                                                },
                                                <?php
                                                if($id_role == 1 || $id_role == 2) {
                                                ?>
                                                {
                                                    "data" : "action_delete",
                                                    "orderable": false,
                                                    "className" : "text-center"
                                                },
                                                <?php
                                                }
                                            } else {

                                            }
                                        ?>
                                        {
					"data": "nama_indikator_ikpa",
                                        "orderable": false,
                                        "className" : "text-left"
					},{
					"data": "jenis_penyebab",
                                        "orderable": false,
                                        "className" : "text-left"
                                        },{
					"data": "uraian_penyebab",
                                        "orderable": false,
                                        "className" : "text-left",
                                        "defaultContent": '<font color="red"><b>Belum Isi Uraian Penyebab</b></font>'
                                        },{
					"data": "jenis_solusi",
                                        "orderable": false,
                                        "className" : "text-left"
                                        },{
                                        "data": "uraian_solusi",
                                        "orderable": false,
                                        "className" : "text-center",
                                        "defaultContent": '<font color="red"><b>Belum Isi Uraian Solusi</b></font>'
                                        }
                                        
            
            ],
            order: [[0, 'asc']],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
        
    });
</script>