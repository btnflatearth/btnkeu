<div class="content-wrapper">
    <section class="content">
        <div class="box box-success box-solid">
            <div class="box-header with-border">
                <h3 class="box-title"><b><?= strtoupper($judul_form) ?></b></h3>
            </div>
            <form action="<?= $aksi ?>" method="post" enctype="multipart/form-data">
                <table class="table table-bordered">
                    <tr>
                        <td style="text-align: right;vertical-align: middle;width: 200px"><strong>Tahun</strong></td>
                        <td style="text-align: center;vertical-align: middle;width: 30px"><strong>:</strong></td>
                        <td>
                            <select class="form-control select2" name="idr_tahun" style="width:130px">
                                <option value="">- Pilih Tahun -</option>
                                <?php foreach($tahunlist as $thn){ ?>
                                    <option <?php if($idr_tahun == $thn->idr_tahun){echo "selected='selected'";} ?> value="<?php echo $thn->idr_tahun ?>"><?php echo $thn->idr_tahun; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                        <td>
                            <?php echo '<b>'.form_error('idr_tahun').'</b>'; ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;vertical-align: middle;width: 200px"><strong>Bulan</strong></td>
                        <td style="text-align: center;vertical-align: middle;width: 30px"><strong>:</strong></td>
                        <td>
                            <select class="form-control select2" name="idr_bulan" style="width:130px">
                                <option value="">- Pilih Bulan -</option>
                                <?php foreach($bulanlist as $bln){ ?>
                                    <option <?php if($idr_bulan == $bln->idr_bulan){echo "selected='selected'";} ?> value="<?php echo $bln->idr_bulan ?>"><?php echo $bln->nama_bulan; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                        <td>
                            <?php echo '<b>'.form_error('idr_bulan').'</b>'; ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;vertical-align: middle;width: 200px"><strong>Satker</strong></td>
                        <td style="text-align: center;vertical-align: middle;width: 30px"><strong>:</strong></td>
                        <td>
                            <select class="form-control select2" name="idr_satker" style="width:500px">
                                <option value="">- Pilih Satker -</option>
                                <?php foreach($satkerlist as $skr){ ?>
                                    <option <?php if($idr_satker == $skr->idr_satker){echo "selected='selected'";} ?> value="<?php echo $skr->idr_satker ?>"><?php echo $skr->nama_satker; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                        <td>
                            <?php echo '<b>'.form_error('idr_satker').'</b>'; ?>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <input type="hidden" name="<?= $id ?>" value="<?php echo $id_value; ?>" /> 
                            <button type="submit" class="btn btn-danger"><i class="fa fa-floppy-o"></i>
                                <?= $tombol_simpan ?>
                            </button> 
                            <a href="<?php echo site_url($controller) ?>" class="btn btn-info"><i class="fa fa-sign-out"></i> <?= $tombol_kembali ?></a>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </section>
</div>