<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-success box-solid">
                    <div class="box-header">
                        <h3 class="box-title"><b><?= strtoupper($judul_list) ?></b></h3>
                    </div>
                    <div class="box-body">
                        <div style="padding-bottom: 10px;">
                            <?php 
                                if($flag_tambah == TRUE) {
                                    echo anchor(site_url($controller.'/tambah/'), '<i class="fa fa-wpforms" aria-hidden="true"></i> Tambah Data', 'class="btn btn-dropbox btn-sm"').' ';
                                }
                                if($flag_export_excel == TRUE) {
                                    echo anchor(site_url($controller.'/excel'), '<i class="fa fa-file-excel-o" aria-hidden="true"></i> Export Ms Excel', 'class="btn btn-success btn-sm"').' ';
                                }
                                if($flag_export_word == TRUE) {
                                    echo anchor(site_url($controller.'/word'), '<i class="fa fa-file-word-o" aria-hidden="true"></i> Export Ms Word', 'class="btn btn-flickr btn-sm"').' '; 
                                }
                            ?>
                        </div>
                        <table class="table table-bordered table-striped" id="mytable">
                            <thead>
                                <tr>
                                    <th style="width:20px;text-align: center">No</th>
                                    <?php if($flag_edit == TRUE) { ?>
                                    <th style="width:40px;text-align: center">Edit</th>
                                    <?php } if($flag_detail == TRUE) { ?>
                                    <th style="width:40px;text-align: center">Detail</th>
                                    <?php } if($flag_delete == TRUE) { ?>
                                    <th style="width:40px;text-align: center">Delete</th>
                                    <?php } ?>
                                    <th style="width:70px;text-align: center">Tahun</th>
                                    <th style="width:70px;text-align: center">Bulan</th>
                                    <th style="width:200px;text-align: center">Kanwil</th>
                                    <th style="width:300px;text-align: center">Satker</th>
                                    <th style="width:80px;text-align: center">Status</th>
                                    <th style="width:80px;text-align: center">Skor IKPA</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#mytable").dataTable({
            "scrollX": true,
            "scrollY": "300px",
            "scrollCollapse": true,
            initComplete: function() {
                var api = this.api();
//                $('#mytable_filter input')
//                        .off('.DT')
//                        .on('keyup.DT', function(e) {
//                            if (e.keyCode == 13) {
//                                api.search(this.value).draw();
//                    }
//                });
                $('#mytable').on( 'keyup', function () {
                    table.search( this.value ).draw();
                });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            ajax: {"url": "<?= $controller ?>/json", "type": "POST"},
            columns: [{
                                        "data": "idt_ikpa_satker",
                                        "className" : "text-center"
                                        },
                                        <?php if($flag_edit == TRUE) { ?>
                                        {
                                            "data" : "action_edit",
                                            "orderable": false,
                                            "className" : "text-center",
                                            render: function ( data, type, row ) {
                                                return row.flag_posting == 1 ?
                                                '' :
                                                '<font color="green"><b>'+data+'</b></font>';
                                            }
                                        },
                                        <?php } if($flag_detail == TRUE) { ?>
                                        {
                                            "data" : "action_detail",
                                            "orderable": false,
                                            "className" : "text-center"
                                        },
                                        <?php } if($flag_delete == TRUE) { ?>
                                        {
                                        "data" : "action_delete",
                                        "orderable": false,
                                        "className" : "text-center",
                                            render: function ( data, type, row ) {
                                                return row.flag_posting == 1 ?
                                                '' :
                                                '<font color="green"><b>'+data+'</b></font>';
                                            }
                                        },
                                        <?php } ?>
					{
					"data": "idr_tahun",
                                        "orderable": false,
                                        "className" : "text-center"
					},{
					"data": "nama_bulan",
                                        "orderable": false,
                                        "className" : "text-center",
                                        },{
					"data": "nama_kanwil_bpn",
                                        "orderable": false,
                                        "className" : "text-left"
                                        },{
					"data": "nama_satker",
                                        "orderable": false,
                                        "className" : "text-left"
                                        },{
                                        "data": "flag_posting",
                                        "orderable": false,
                                        "className" : "text-center",
                                        render: function ( data, type, row ) {
                                            switch(data) {
                                                case '1' : return '<font color="green"><b>FINAL</b></font>'; break;
                                                case '0' : return '<font color="red"><b>DRAFT</b></font>'; break;
                                                default : return 'NO-DATA';
                                            }
                                        }
                                        },{
                                        "data": "nilai_akhir_ikpa",
                                        "orderable": false,
                                        "className" : "text-center",
                                        render: function ( data, type, row ) {
                                            return data < 90 ?
                                            '<font color="red"><b>'+row.nilai_akhir_ikpa+' %</b></font>' :
                                            '<font color="green"><b>'+row.nilai_akhir_ikpa+' %</b></font>';
                                        }
                                    },
                                        
            
            ],
            order: [[0, 'asc']],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
        
    });
</script>