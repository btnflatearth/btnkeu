<?php 
$row = $this->m_data->get_by_id('t_ikpa_satker', 'idt_ikpa_satker',$id);
$val = $this->m_data->get_by_id('v_ikpa_satker_val', 'idt_ikpa_satker',$id);
$id_role = $this->session->userdata('id_role');
$val_total = 1;
if($row->flag_posting == 1) {
    $narasi = 'Terima Kasih telah menggunakan Aplikasi Semesta !!!';
} else {
    $narasi = 'Untuk <b>FINALKAN</b> data silahkan Klik Tombol <b>POSTING</b>';
}
if($val->val_nilai_akhir_ikpa == 1) {
    $val1 = 1;
    $val1_msg = '';
} else {
    if($val->val_evaluasi == 1) {
        $val1 = 1;
        $val1_msg = '';
    } else {
        $val1 = 0;
        $val1_msg = '<font color="red"><b>Nilai IKPA dibawah 90 %, Diwajibkan mengisi Penyebab dan Solusi !!!</b></font>';
    }
}
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-success box-solid">
                <div class="box-header">
                    <h3 class="box-title"><b><?= strtoupper($judul_final) ?></b></h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <td style="text-align: right;vertical-align: middle;width: 200px"><strong>Status</strong></td>
                            <td style="text-align: center;vertical-align: middle;width: 30px"><strong>:</strong></td>
                            <td><?php if($row->flag_posting == 1) { echo '<font color="blue"><b>FINAL</b></font>'; } else { echo '<font color="red"><b>DRAFT</b></font>'; } ?></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <?php 
                                if(($val1) == $val_total) {
                                    
                                } else {
                                    echo '<font color="red"><b>Masih ada data yang belum VALID !!!</b></font><hr>';
                                    echo '<ul>';
                                    if($val1 == 0) {
                                        echo '<li>'.$val1_msg.'</li>';
                                    }
                                    echo '</ul>';
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <p><?= $narasi ?></p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <?php 
                                    if($id_role == 1 || $id_role == 2) {
                                        if($row->flag_posting == 0) {
                                            if(($val1) == $val_total) {
                                                echo anchor(site_url($controller.'/final/'.$row->idt_ikpa_satker.'/final'), '<i class="fa fa-arrow-right" aria-hidden="true"></i> POSTING', 'class="btn btn-dropbox btn-sm" onclick="javasciprt: return confirm(\'Yakin mau melakukan POSTING data? Data yang sudah di POSTING tidak dapat dirubah !!!\')"').' ';
                                            }
                                        } else {
                                            echo anchor(site_url($controller.'/draft/'.$row->idt_ikpa_satker.'/final'), '<i class="fa fa-arrow-left" aria-hidden="true"></i> DRAFTKAN', 'class="btn btn-dropbox btn-sm" onclick="javasciprt: return confirm(\'Yakin mau melakukan DRAFT data?\')"').' ';
                                        }
                                    } else {
                                        if($row->flag_posting == 0) {
                                            if(($val1) == $val_total) {
                                                echo anchor(site_url($controller.'/final/'.$row->idt_ikpa_satker.'/final'), '<i class="fa fa-arrow-right" aria-hidden="true"></i> POSTING', 'class="btn btn-dropbox btn-sm" onclick="javasciprt: return confirm(\'Yakin mau melakukan POSTING data? Data yang sudah di POSTING tidak dapat dirubah !!!\')"').' ';
                                            }
                                        }
                                    }
                                ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>