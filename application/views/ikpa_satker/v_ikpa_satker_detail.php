<?php 
$row = $this->m_data->get_by_id('t_ikpa_satker', 'idt_ikpa_satker',$id);
?>
<div class="content-wrapper">
    <section class="content">
        <div class="box box-success box-solid">
            <div class="box-header with-border">
                <h3 class="box-title"><b><?= $judul.' '.strtoupper($row->nama_satker).' TAHUN '.$row->idr_tahun.' BULAN '.strtoupper($row->nama_bulan) ?></b></h3>
            </div>
            <table class="table table-bordered">
                <tr>
                    <td colspan="3">
                        <a href="<?php echo site_url($controller) ?>" class="btn btn-info"><i class="fa fa-sign-out"></i> <?= $tombol_kembali ?></a>
                    </td>
                </tr>
                <tr>
                    <td style="width: 200px;text-align: right;vertical-align: middle"><strong>Tahun / Bulan</strong></td>
                    <td style="width: 10px;text-align: center;vertical-align: middle"><strong>&nbsp;&nbsp;:&nbsp;&nbsp;</strong></td>
                    <td style="vertical-align: middle"><?= $row->idr_tahun.' / '.$row->nama_bulan ?></td>
                </tr>
                <tr>
                    <td style="width: 200px;text-align: right;vertical-align: middle"><strong>Kanwil</strong></td>
                    <td style="width: 10px;text-align: center;vertical-align: middle"><strong>&nbsp;&nbsp;:&nbsp;&nbsp;</strong></td>
                    <td style="vertical-align: middle"><?= $row->nama_kanwil_bpn ?></td>
                </tr>
                <tr>
                    <td style="width: 200px;text-align: right;vertical-align: middle"><strong>Satker</strong></td>
                    <td style="width: 10px;text-align: center;vertical-align: middle"><strong>&nbsp;&nbsp;:&nbsp;&nbsp;</strong></td>
                    <td style="vertical-align: middle"><?= $row->nama_satker ?></td>
                </tr>
                <tr>
                    <td style="width: 200px;text-align: right;vertical-align: middle"><strong>Skor IKPA</strong></td>
                    <td style="width: 10px;text-align: center;vertical-align: middle"><strong>&nbsp;&nbsp;:&nbsp;&nbsp;</strong></td>
                    <td style="vertical-align: middle"><?php if($row->nilai_akhir_ikpa < 90.00) { echo '<font color="red"><b>'.$row->nilai_akhir_ikpa.' %</b></font>'; } else { echo '<font color="green"><b>'.$row->nilai_akhir_ikpa.' %</b></font>'; } ?></td>
                </tr>
                <tr>
                    <td style="width: 200px;text-align: right;vertical-align: middle"><strong>Status</strong></td>
                    <td style="width: 10px;text-align: center;vertical-align: middle"><strong>&nbsp;&nbsp;:&nbsp;&nbsp;</strong></td>
                    <td style="vertical-align: middle"><?php if($row->flag_posting == 1) { echo '<font color="blue"><b>FINAL</b></font>'; } else { echo '<font color="red"><b>DRAFT</b></font>'; } ?></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <a href="<?php echo site_url($controller) ?>" class="btn btn-info"><i class="fa fa-sign-out"></i> <?= $tombol_kembali ?></a>
                    </td>
                </tr>
            </table>
            <table class="table table-bordered">
                <tr>
                    <td>
                        <a href="<?php echo site_url($controller.'/detail/'.$id.'/final') ?>" class="btn btn-<?php if($string_menu == 'final') { echo 'danger'; } else { echo 'microsoft';} ?>"><i class="fa fa-list"></i> FINALKAN DATA</a>
                        <a href="<?php echo site_url($controller.'/detail/'.$id.'/penyebab') ?>" class="btn btn-<?php if($string_menu == 'penyebab') { echo 'danger'; } else { echo 'microsoft';} ?>"><i class="fa fa-list"></i> INPUT PENYEBAB</a>
                    </td>
                </tr>
            </table>
            <?php 
                if($string_menu == 'final') { 
                    $this->load->view('ikpa_satker/v_ikpa_satker_final');
                }
                if($string_menu == 'penyebab') { 
                    $this->load->view('ikpa_satker/v_ikpa_satker_penyebab_list');
                }
            ?>
        </div>
    </section>
</div>