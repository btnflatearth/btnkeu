<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?= $judul.' Aplikasi '. getInfoAPP('app_nama') ?>
            <small><?= getInfoAPP('app_deskripsi') ?></small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-success">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <td> <strong>Panduan Penggunaan Aplikasi <?= getInfoAPP('app_nama') ?></strong><br/>
                                        
                                    </td>
                                </tr>
                                <tr><td><br/><br/><br/></td></tr>
                                <tr>
                                    <td style="text-align: center">
                                        <?= getInfoAPP('about_text') ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>