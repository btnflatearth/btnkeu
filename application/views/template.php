<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo getInfoAPP('app_title') ?></title>
        <link rel="icon" type="ico" href="<?php echo base_url(); ?>assets/img/logo_bpn.png">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/jquery-ui/themes/base/minified/jquery-ui.min.css" type="text/css" />
        <link rel="stylesheet" href="<?= base_url() ?>assets/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/adminlte/bower_components/select2/dist/css/select2.min.css">
        <!--<link href="<?= base_url() ?>assets/select2/dist/css/select2.min.css" rel="stylesheet" />-->
        <link rel="stylesheet" href="<?= base_url() ?>assets/adminlte/bower_components/Ionicons/css/ionicons.min.css">
        <!-- DataTables -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
        <!-- jvectormap 
        <link rel="stylesheet" href="<?= base_url() ?>https://adminlte.io/themes/AdminLTE/bower_components/jvectormap/jquery-jvectormap.css">
        -->
        <!-- Theme style -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/adminlte/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/adminlte/dist/css/skins/_all-skins.min.css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="<?= base_url() ?>https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="<?= base_url() ?>https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <!-- Highcharts -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/highcharts/code/css/highcharts.css">
        
        <!-- Highcharts -->
        <script src="<?= base_url() ?>assets/highcharts/code/highcharts.js"></script>
        <script src="<?= base_url() ?>assets/highcharts/code/modules/series-label.js"></script>
        <script src="<?= base_url() ?>assets/highcharts/code/modules/exporting.js"></script>
        <script src="<?= base_url() ?>assets/highcharts/code/modules/export-data.js"></script>
        <script src="<?= base_url() ?>assets/highcharts/code/modules/accessibility.js"></script>
        
        <style type="text/css">
            .ui-autocomplete {
                position: absolute;
                z-index: 2150000000 !important;
                cursor: default;
                border: 2px solid #ccc;
                padding: 5px 0;
                border-radius: 2px;
            }
        </style>
        <!-- Google Font -->
        <link rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="<?= base_url() ?>/C_auth" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><img src="<?= base_url() ?>assets/img/icon_sadewa-03.png" width="20px" height="20px"></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b><?php echo getInfoAPP('app_nama') ?></b></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="<?= base_url() ?>#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">                          
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="<?= base_url() ?>#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?= base_url() ?>assets/img/blank.png" class="user-image" alt="User Image">
                                    <span class="hidden-xs"><?php echo '<b>'.$this->session->userdata('name').' || '.strtoupper($this->session->userdata('grup')).' || '; ?></span>
                                    <?php echo "<span>".hari_ini(date('w')).", ".tgl_indo(date('Y-m-d'))." || <span id='jam'></span></b>"; ?>
                                </a>
                                
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?= base_url() ?>assets/img/blank.png" class="img-circle" alt="User Image">
                                        <p>
                                            <?php echo $this->session->userdata('nama'); ?>                                         
                                            <small>User dibuat : <?php echo tgl_indo($this->session->userdata('created_date'), "d-m-Y"); ?>  </small>
                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div align="center">
                                            <a href="<?= base_url() ?>assets/file/sadewa_v1.1.7.apk" target="_blank">
                                                Download APK<br><img src="<?= base_url() ?>assets/img/apk.png" width="50px" height="50px"><hr>
                                            </a>
                                        </div>
                                        <div align="center">
                                            <?php echo anchor('C_profile', 'Profile', array('class' => 'btn btn-default btn-flat')); ?>
                                            <!--<a href="<?= base_url() ?>#" class="btn btn-default btn-flat">Profile</a>-->
                                            <?php echo anchor('C_about', 'About', array('class' => 'btn btn-default btn-flat')); ?>
                                            <!--<a href="<?= base_url() ?>#" class="btn btn-default btn-flat">Profile</a>-->
                                            <?php echo anchor('logout', 'Logout', array('class' => 'btn btn-default btn-flat')); ?>
                                            <!--<a href="<?= base_url() ?>#" class="btn btn-default btn-flat">Sign out</a>-->
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                            <li>
                                <a href="<?= base_url() ?>#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <?php $this->load->view('template/sidebar'); ?>
            </aside>
            <?php
            echo $contents;
            ?>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b><?php echo getInfoAPP('app_deskripsi') ?></b>
                </div>
                <strong><?php echo getInfoAPP('copyright') ?></strong>
            </footer>
            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Create the tabs -->
                <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                    <li><a href="<?= base_url() ?>#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
                    <li><a href="<?= base_url() ?>#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <!-- Home tab content -->
                    <div class="tab-pane" id="control-sidebar-home-tab">
                        <h3 class="control-sidebar-heading">Recent Activity</h3>
                        <ul class="control-sidebar-menu">
                            <li>
                                <a href="<?= base_url() ?>javascript:void(0)">
                                    <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
                                        <p>Will be 23 on April 24th</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>javascript:void(0)">
                                    <i class="menu-icon fa fa-user bg-yellow"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>
                                        <p>New phone +1(800)555-1234</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>javascript:void(0)">
                                    <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                                        <p>nora@example.com</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>javascript:void(0)">
                                    <i class="menu-icon fa fa-file-code-o bg-green"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                                        <p>Execution time 5 seconds</p>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <!-- /.control-sidebar-menu -->

                        <h3 class="control-sidebar-heading">Tasks Progress</h3>
                        <ul class="control-sidebar-menu">
                            <li>
                                <a href="<?= base_url() ?>javascript:void(0)">
                                    <h4 class="control-sidebar-subheading">
                                        Custom Template Design
                                        <span class="label label-danger pull-right">70%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>javascript:void(0)">
                                    <h4 class="control-sidebar-subheading">
                                        Update Resume
                                        <span class="label label-success pull-right">95%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>javascript:void(0)">
                                    <h4 class="control-sidebar-subheading">
                                        Laravel Integration
                                        <span class="label label-warning pull-right">50%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>javascript:void(0)">
                                    <h4 class="control-sidebar-subheading">
                                        Back End Framework
                                        <span class="label label-primary pull-right">68%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <!-- /.control-sidebar-menu -->

                    </div>
                    <!-- /.tab-pane -->
                    <!-- Stats tab content -->
                    <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
                    <!-- /.tab-pane -->
                    <!-- Settings tab content -->
                    <div class="tab-pane" id="control-sidebar-settings-tab">
                        <form method="post">
                            <h3 class="control-sidebar-heading">General Settings</h3>

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Report panel usage
                                    <input type="checkbox" class="pull-right" checked>
                                </label>

                                <p>
                                    Some information about this general settings option
                                </p>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Allow mail redirect
                                    <input type="checkbox" class="pull-right" checked>
                                </label>

                                <p>
                                    Other sets of options are available
                                </p>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Expose author name in posts
                                    <input type="checkbox" class="pull-right" checked>
                                </label>

                                <p>
                                    Allow the user to show his name in blog posts
                                </p>
                            </div>
                            <!-- /.form-group -->

                            <h3 class="control-sidebar-heading">Chat Settings</h3>

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Show me as online
                                    <input type="checkbox" class="pull-right" checked>
                                </label>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Turn off notifications
                                    <input type="checkbox" class="pull-right">
                                </label>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Delete chat history
                                    <a href="<?= base_url() ?>javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
                                </label>
                            </div>
                            <!-- /.form-group -->
                        </form>
                    </div>
                    <!-- /.tab-pane -->
                </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/jquery-ui/ui/minified/jquery-ui.min.js"></script>
        <!-- jQuery 3
        <script src="<?= base_url() ?>assets/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
        -->
        <!-- Bootstrap 3.3.7 -->
        <script src="<?= base_url() ?>assets/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- DataTables -->
        <script src="<?= base_url() ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?= base_url() ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <!-- SlimScroll -->
        <script src="<?= base_url() ?>assets/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="<?= base_url() ?>assets/adminlte/bower_components/fastclick/lib/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="<?= base_url() ?>assets/adminlte/dist/js/adminlte.min.js"></script>
        <!-- ChartJS -->
        <script src="<?= base_url() ?>assets/adminlte/bower_components/chart.js/Chart.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?= base_url() ?>assets/adminlte/dist/js/demo.js"></script>
        

        <!-- jvectormap  -->
        <script src="https://adminlte.io/themes/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="https://adminlte.io/themes/AdminLTE/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>

        <!-- Select2 -->
        <script src="<?= base_url() ?>assets/adminlte/bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="<?= base_url() ?>assets/ckeditor/ckeditor.js"></script>
        <!--<script src="<?= base_url() ?>assets/select2/dist/js/select2.min.js"></script>-->
        
        <script type="text/javascript">
        $(document).ready(function() {
            $('.select2').select2();
        });
        </script>
        
        <script>
            $(function () {
              CKEDITOR.replace('ringkasan');
              CKEDITOR.config.width = 500;
              CKEDITOR.config.height = 200;
            });
        </script>
        
        <!-- page script -->
        <script>
            $(function () {
                $('.select2').select2()
                $('#example1').DataTable()
                $('#example2').DataTable({
                    'paging'      : true,
                    'lengthChange': false,
                    'searching'   : false,
                    'ordering'    : true,
                    'info'        : true,
                    'autoWidth'   : false
                })
            })
        </script>
        <script type="text/javascript">
            function jam(){
                var waktu = new Date();
                var jam = waktu.getHours();
                var menit = waktu.getMinutes();
                var detik = waktu.getSeconds();

                if (jam < 10){ jam = "0" + jam; }
                if (menit < 10){ menit = "0" + menit; }
                if (detik < 10){ detik = "0" + detik; }
                var jam_div = document.getElementById('jam');
                jam_div.innerHTML = jam + ":" + menit + ":" + detik;
                setTimeout("jam()", 1000);
            } jam();
        </script>
    </body>
</html>
