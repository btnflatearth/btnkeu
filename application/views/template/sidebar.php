<section class="sidebar">
    <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
            </span>
        </div>
    </form>
    <div class="user-panel">
        <div class="pull-left image">
            <img src="<?php echo base_url() ?>assets/img/blank.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
            <p><span><?= $this->session->userdata('nama') ?></span></p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
    </div>
    <br>
    <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU UTAMA</li>
        <li><?php echo anchor('c_dashboard/kosong/ksg',"<i class='fa fa-calculator'></i> <span>DASHBOARD</span>");?></li>
        <?php 
        $role_code = $this->session->userdata('grup');
        if(getInfoAPP('flag_offline') == 1) {
            $menu_parent = $this->m_data->get_data_filter_menu('r_menu',"idr_parent IS NULL AND menu_status = 'AKTIF' AND menu_akses_name LIKE '%".$role_code."%' AND menu_status_offline = 'AKTIF'",'menu_order','ASC')->result_array();
        } else {
            $menu_parent = $this->m_data->get_data_filter_menu('r_menu',"idr_parent IS NULL AND menu_status = 'AKTIF' AND menu_akses_name LIKE '%".$role_code."%'",'menu_order','ASC')->result_array();
        }
        foreach ($menu_parent as $parent) {
            if($parent['menu_parent_status'] == 'YA') {
        ?>
        <li class='treeview'>
            <a href='#'>
                <i class="fa <?= $parent['menu_icon'] ?>"></i><span><?= strtoupper($parent['menu_name']); ?></span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu" style="display: none;">
            <?php  
            $idr_parent = $parent['idr_menu'];
            if(getInfoAPP('flag_offline') == 1) {
                $menu_child = $this->m_data->get_data_filter_menu('r_menu',"idr_parent IS NOT NULL AND idr_parent = $idr_parent AND menu_status = 'AKTIF' AND menu_akses_name LIKE '%".$role_code."%'",'menu_order','ASC')->result_array();
            } else {
                $menu_child = $this->m_data->get_data_filter_menu('r_menu',"idr_parent IS NOT NULL AND idr_parent = $idr_parent AND menu_status = 'AKTIF' AND menu_akses_name LIKE '%".$role_code."%' AND menu_status_offline = 'AKTIF'",'menu_order','ASC')->result_array();
            }
            foreach ($menu_child as $child) {
            ?>
            <li><?php echo anchor($child['menu_controller'],"<i class='fa ".$child['menu_icon']."'></i> ".strtoupper($child['menu_name'])); ?></li>
            <?php } ?>
            </ul>
        </li>
        <?php
            } else {
        ?>
        <li><?php echo anchor($parent['menu_controller'],"<i class='fa ".$parent['menu_icon']."'></i> <span>".strtoupper($parent['menu_name'])."</span>");?></li>
        <?php
            }
        }
        ?>
        <li><?php echo anchor('logout',"<i class='fa fa-sign-out'></i> <span>LOGOUT</span>");?></li>
    </ul>
</section>