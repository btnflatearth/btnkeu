<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title"><strong><?= $judul_akses_unit ?></strong></h3>
                <div class="box-tools pull-right">
                    <!--<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>-->
                </div>
                <div class="box-body">
                    <div style="padding-bottom: 10px;">
                        <?php 
                        echo anchor(site_url($controller.'/tambah_akses_unit/'.$id), '<i class="fa fa-wpforms" aria-hidden="true"></i> Tambah Data', 'class="btn btn-dropbox btn-sm"').' ';
                        ?>
                    </div>
                    <div class="row">
                        <table class="table table-bordered table-striped" id="mytable_akses">
                            <thead>
                                <tr>
                                    <th style="width:20px;text-align: center">No</th>
                                    <th style="width:500px;text-align: center">Satker</th>
                                    <th style="width:40px;text-align: center">Edit</th>
                                    <th style="width:40px;text-align: center">Delete</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#mytable_akses").dataTable({
            "scrollX": true,
            "scrollY": "300px",
            "scrollCollapse": true,
            initComplete: function() {
                var api = this.api();
                $('#mytable_akses_filter input')
                        .off('.DT')
                        .on('keyup.DT', function(e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                    }
                });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            ajax: {"url": "../../<?= $controller ?>/json_akses_unit/<?= $id ?>", "type": "POST"},
            columns: [{
                                        "data": "idt_user_satker",
                                        "className" : "text-center"
                                        },{
					"data": "nama_satker",
                                        "orderable": false,
                                        "className" : "text-left"
					},{
                                            "data" : "action_edit",
                                            "orderable": false,
                                            "className" : "text-center"
                                        },{
                                            "data" : "action_delete",
                                            "orderable": false,
                                            "className" : "text-center"
                                        }
            ],
            order: [[1, 'asc']],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
        
    });
</script>