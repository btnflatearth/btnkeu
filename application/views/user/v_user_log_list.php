<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title"><strong><?= $judul_log ?></strong></h3>
                <div class="box-tools pull-right">
                    <!--<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>-->
                </div>
                <div class="box-body">
                    <div class="row">
                        <table class="table table-bordered table-striped" id="mytable_log">
                            <thead>
                                <tr>
                                    <th style="width:20px;text-align: center">No</th>
                                    <th style="width:200px;text-align: center">Username</th>
                                    <th style="width:600px;text-align: center">Log</th>
                                    <th style="width:150px;text-align: center">Date</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#mytable_log").dataTable({
            "scrollX": true,
            "scrollY": "300px",
            "scrollCollapse": true,
            initComplete: function() {
                var api = this.api();
                $('#mytable_log_filter input')
                        .off('.DT')
                        .on('keyup.DT', function(e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                    }
                });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            ajax: {"url": "../../<?= $controller ?>/json_log/<?= $id ?>", "type": "POST"},
            columns: [{
                                        "data": "idt_log_user",
                                        "className" : "text-center"
                                        },{
					"data": "name",
                                        "orderable": false,
                                        "className" : "text-left"
					},{
					"data": "log_user",
                                        "orderable": false,
                                        "className" : "text-left"
					},{
					"data": "created_date",
                                        "orderable": false,
                                        "className" : "text-left"
					}
            ],
            order: [[3, 'desc']],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
        
    });
</script>