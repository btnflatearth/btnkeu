<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-danger box-solid">
                    <div class="box-header">
                        <h3 class="box-title"><b><?= strtoupper($judul_list) ?></b></h3>
                    </div>
                    <div class="box-body">
                        <div style="padding-bottom: 10px;">
                            <?php 
                                if($flag_tambah == TRUE) {
                                    echo anchor(site_url($controller.'/tambah/'), '<i class="fa fa-wpforms" aria-hidden="true"></i> Tambah Data', 'class="btn btn-dropbox btn-sm"').' ';
                                }
                                if($flag_export_excel == TRUE) {
                                    echo anchor(site_url($controller.'/excel'), '<i class="fa fa-file-excel-o" aria-hidden="true"></i> Export Ms Excel', 'class="btn btn-success btn-sm"').' ';
                                }
                                if($flag_export_word == TRUE) {
                                    echo anchor(site_url($controller.'/word'), '<i class="fa fa-file-word-o" aria-hidden="true"></i> Export Ms Word', 'class="btn btn-flickr btn-sm"').' '; 
                                }
                            ?>
                        </div>
                        <table class="table table-bordered table-striped" id="mytable">
                            <thead>
                                <tr>
                                    <th style="width:20px;text-align: center">No</th>
                                    <?php if($flag_edit == TRUE) { ?>
                                    <th style="width:40px;text-align: center">Edit</th>
                                    <?php } if($flag_detail == TRUE) { ?>
                                    <th style="width:40px;text-align: center">Detail</th>
                                    <?php } if($flag_delete == TRUE) { ?>
                                    <th style="width:40px;text-align: center">Delete</th>
                                    <?php } ?>
                                    <th style="width:150px;text-align: center">Email</th>
                                    <th style="width:150px;text-align: center">Nama</th>
                                    <th style="width:250px;text-align: center">Unit Kerja Detail</th>
                                    <th style="width:250px;text-align: center">Unit Kerja</th>
                                    <th style="width:80px;text-align: center">Status</th>
                                    <th style="width:80px;text-align: center">Role Name</th>
                                    <th style="width:80px;">Last Login</th>
                                    <th style="width:350px;text-align: center">Akses Unit</th>
                                    <th style="width:350px;text-align: center">Log</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#mytable").dataTable({
            "scrollX": true,
            "scrollY": "300px",
            "scrollCollapse": true,
            initComplete: function() {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function(e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                    }
                });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            ajax: {"url": "<?= $controller ?>/json", "type": "POST"},
            columns: [{
                                        "data": "idt_user",
                                        "className" : "text-center"
                                        },
                                        <?php if($flag_edit == TRUE) { ?>
                                        {
                                            "data" : "action_edit",
                                            "orderable": false,
                                            "className" : "text-center"
                                        },
                                        <?php } if($flag_detail == TRUE) { ?>
                                        {
                                            "data" : "action_detail",
                                            "orderable": false,
                                            "className" : "text-center"
                                        },
                                        <?php } if($flag_delete == TRUE) { ?>
                                        {
                                        "data" : "action_delete",
                                        "orderable": false,
                                        "className" : "text-center"
                                        },
                                        <?php } ?>
					{
					"data": "email",
                                        "orderable": false,
                                        "className" : "text-left"
					},{
					"data": "nama",
                                        "orderable": false,
                                        "className" : "text-left"
                                        },{
					"data": "unit_kerja_peg",
                                        "orderable": false,
                                        "className" : "text-left"
                                        },{
					"data": "unit_kerja_es2",
                                        "orderable": false,
                                        "className" : "text-left"
					},{
                                        "data": "status",
                                        "orderable": false,
                                        "className" : "text-center",
                                        render: function ( data, type, row ) {
                                            switch(data) {
                                                case '1' : return '<font color="green"><b>AKTIF</b></font>'; break;
                                                case '0' : return '<font color="red"><b>BANNED</b></font>'; break;
                                                default : return 'NO-DATA';
                                            }
                                        }
					},{
                                        "data": "role_name",
                                        "orderable": false,
                                        "className" : "text-center"
					},{
                                        "data": "last_login",
                                        "orderable": false,
                                        "className" : "text-center"
                                        },{
                                        "data": "akses_unit",
                                        "orderable": false,
                                        "className" : "text-left"
					},{
                                        "data": "log",
                                        "orderable": false,
                                        "className" : "text-left"
					},
                                        
            
            ],
            order: [[5, 'asc']],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
        
    });
</script>