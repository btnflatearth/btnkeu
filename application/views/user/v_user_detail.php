<?php 
$row = $this->m_data->get_by_id('v_users', 'id',$id);
?>
<div class="content-wrapper">
    <section class="content">
        <div class="box box-success box-solid">
            <div class="box-header with-border">
                <h3 class="box-title"><b><?= $judul ?></b></h3>
            </div>
            <table class="table table-bordered">
                <tr>
                    <td colspan="3">
                        <a href="<?php echo site_url($controller) ?>" class="btn btn-info"><i class="fa fa-sign-out"></i> <?= $tombol_kembali ?></a>
                    </td>
                </tr>
                <tr>
                    <td style="width: 200px;text-align: right;vertical-align: middle"><strong>Nama</strong></td>
                    <td style="width: 10px;text-align: center;vertical-align: middle"><strong>&nbsp;&nbsp;:&nbsp;&nbsp;</strong></td>
                    <td style="vertical-align: middle"><?= $row->name ?></td>
                </tr>
                <tr>
                    <td style="width: 200px;text-align: right;vertical-align: middle"><strong>Email</strong></td>
                    <td style="width: 10px;text-align: center;vertical-align: middle"><strong>&nbsp;&nbsp;:&nbsp;&nbsp;</strong></td>
                    <td style="vertical-align: middle"><?= $row->email ?></td>
                </tr>
                <tr>
                    <td style="width: 200px;text-align: right;vertical-align: middle"><strong>Role</strong></td>
                    <td style="width: 10px;text-align: center;vertical-align: middle"><strong>&nbsp;&nbsp;:&nbsp;&nbsp;</strong></td>
                    <td style="vertical-align: middle">
                        <?= $row->grup.' ' ?>
                        <a href="<?php echo site_url($controller.'/edit_role/'.$id) ?>" class="btn btn-info"><i class="fa fa-edit"></i> Ubah Role</a>
                    </td>
                </tr>
                <tr>
                    <td style="width: 200px;text-align: right;vertical-align: middle"><strong>Status</strong></td>
                    <td style="width: 10px;text-align: center;vertical-align: middle"><strong>&nbsp;&nbsp;:&nbsp;&nbsp;</strong></td>
                    <td style="vertical-align: middle">
                        <?php 
                        if($row->status == 'Aktif') {
                            echo '<font color="green"><b>Aktif</b></font>';
                        } else {
                            echo '<font color="red"><b>Banned</b></font>';
                        }
                        ?>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <?php 
                        if($row->status == 'Aktif') {
                        ?>
                        <a href="<?php echo site_url($controller.'/user_off/'.$id) ?>" class="btn btn-danger"><i class="fa fa-angle-down"></i> OFF</a>
                        <?php
                        } else {
                        ?>
                        <a href="<?php echo site_url($controller.'/user_on/'.$id) ?>" class="btn btn-success"><i class="fa fa-angle-up"></i> ON</a>
                        <?php
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="width: 200px;text-align: right;vertical-align: middle"><strong>Last Login</strong></td>
                    <td style="width: 10px;text-align: center;vertical-align: middle"><strong>&nbsp;&nbsp;:&nbsp;&nbsp;</strong></td>
                    <td style="vertical-align: middle"><?= date('d-m-Y H:m:s', strtotime($row->last_login)) ?></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <a href="<?php echo site_url($controller) ?>" class="btn btn-info"><i class="fa fa-sign-out"></i> <?= $tombol_kembali ?></a>
                    </td>
                </tr>
            </table>
            <?php $this->load->view('user/v_user_akses_unit_list') ?>
            <?php $this->load->view('user/v_user_log_list') ?>
        </div>
    </section>
</div>