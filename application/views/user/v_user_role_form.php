<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title"><strong><?= $judul_form ?></strong></h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <div class="box-body">
                            <form action="<?= $aksi ?>" method="post" enctype="multipart/form-data">
                                <table class="table table-bordered table-striped">
                                    <tr>
                                        <td style="text-align: right;vertical-align: middle;width: 200px"><strong>Nama Pegawai</strong></td>
                                        <td style="text-align: center;vertical-align: middle;width: 30px"><strong>:</strong></td>
                                        <td>
                                            <select class="form-control select2" name="group_id" style="width:300px">
                                                <option value="">- Pilih Role -</option>
                                                <?php foreach($rolelist as $rl){ ?>
                                                    <option <?php if($group_id == $rl->id){echo "selected='selected'";} ?> value="<?php echo $rl->id ?>"><?php echo $rl->name.' - '.$rl->title; ?></option>
                                                <?php } ?>
                                            </select>
                                        </td>
                                        <td>
                                            <?php echo '<b>'.form_error('group_id').'</b>'; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <input type="hidden" name="<?= $id ?>" value="<?php echo $user_id; ?>" /> 
                                            <button type="submit" class="btn btn-danger"><i class="fa fa-floppy-o"></i>
                                                <?= $tombol_simpan ?>
                                            </button> 
                                            <a href="<?php echo site_url($controller.'/detail/'.$user_id) ?>" class="btn btn-info"><i class="fa fa-sign-out"></i> <?= $tombol_kembali ?></a>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>