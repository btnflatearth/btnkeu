<?php 
$row = $this->m_data->get_by_id('zzz_v_user', 'idt_user',$idt_user);
?>
<div class="content-wrapper">
    <section class="content">
        <div class="box box-danger box-solid">
            <div class="box-header with-border">
                <h3 class="box-title"><b><?= $judul ?></b></h3>
            </div>
            <table class="table table-bordered">
                <tr>
                    <td colspan="3">
                        <a href="<?php echo site_url($controller) ?>" class="btn btn-info"><i class="fa fa-sign-out"></i> <?= $tombol_kembali ?></a>
                    </td>
                </tr>
                <tr>
                    <td style="width: 200px;text-align: right;vertical-align: middle"><strong>Nama</strong></td>
                    <td style="width: 10px;text-align: center;vertical-align: middle"><strong>&nbsp;&nbsp;:&nbsp;&nbsp;</strong></td>
                    <td style="vertical-align: middle"><?= $row->nama ?></td>
                </tr>
                <tr>
                    <td style="width: 200px;text-align: right;vertical-align: middle"><strong>Email</strong></td>
                    <td style="width: 10px;text-align: center;vertical-align: middle"><strong>&nbsp;&nbsp;:&nbsp;&nbsp;</strong></td>
                    <td style="vertical-align: middle"><?= $row->email ?></td>
                </tr>
                <tr>
                    <td style="width: 200px;text-align: right;vertical-align: middle"><strong>Unit Kerja</strong></td>
                    <td style="width: 10px;text-align: center;vertical-align: middle"><strong>&nbsp;&nbsp;:&nbsp;&nbsp;</strong></td>
                    <td style="vertical-align: middle"v><?= $row->unit_kerja_es2 ?></td>
                </tr>
                <tr>
                    <td style="width: 200px;text-align: right"><strong>Unit Kerja Detail</strong></td>
                    <td style="width: 10px;text-align: center"><strong>&nbsp;&nbsp;:&nbsp;&nbsp;</strong></td>
                    <td style="vertical-align: middle"><?= $row->unit_kerja_peg ?></td>
                </tr>
                <tr>
                    <td style="width: 200px;text-align: right;vertical-align: middle"><strong>Role</strong></td>
                    <td style="width: 10px;text-align: center;vertical-align: middle"><strong>&nbsp;&nbsp;:&nbsp;&nbsp;</strong></td>
                    <td style="vertical-align: middle">
                        <?= $row->role_code.' - '.$row->role_name.' ' ?>
                        <a href="<?php echo site_url($controller.'/edit_role/'.$idt_user) ?>" class="btn btn-info"><i class="fa fa-edit"></i> Ubah Role</a>
                    </td>
                </tr>
                <tr>
                    <td style="width: 200px;text-align: right;vertical-align: middle"><strong>Status</strong></td>
                    <td style="width: 10px;text-align: center;vertical-align: middle"><strong>&nbsp;&nbsp;:&nbsp;&nbsp;</strong></td>
                    <td style="vertical-align: middle">
                        <?php 
                        if($row->status == 1) {
                            echo '<font color="green"><b>ON</b></font>';
                        } else {
                            echo '<font color="red"><b>OFF</b></font>';
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="width: 200px;text-align: right;vertical-align: middle"><strong>Last Login</strong></td>
                    <td style="width: 10px;text-align: center;vertical-align: middle"><strong>&nbsp;&nbsp;:&nbsp;&nbsp;</strong></td>
                    <td style="vertical-align: middle"><?= date('d-m-Y H:m:s', strtotime($row->last_login)) ?></td>
                </tr>
                <tr>
                    <td style="width: 200px;text-align: right;vertical-align: middle"><strong>Akses Unit</strong></td>
                    <td style="width: 10px;text-align: center;vertical-align: middle"><strong>&nbsp;&nbsp;:&nbsp;&nbsp;</strong></td>
                    <td style="vertical-align: middle"><?= $row->akses_unit ?></td>
                </tr>
                <tr>
                    <td style="width: 200px;text-align: right;vertical-align: middle"><strong>Log</strong></td>
                    <td style="width: 10px;text-align: center;vertical-align: middle"><strong>&nbsp;&nbsp;:&nbsp;&nbsp;</strong></td>
                    <td style="vertical-align: middle"><?= $row->log ?></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <a href="<?php echo site_url($controller) ?>" class="btn btn-info"><i class="fa fa-sign-out"></i> <?= $tombol_kembali ?></a>
                    </td>
                </tr>
            </table>
            <?php $this->load->view('user/v_user_akses_unit_list') ?>
        </div>
    </section>
</div>