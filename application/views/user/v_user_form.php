<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title"><strong><?= $judul_form ?></strong></h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <div class="box-body">
                            <form action="<?= $aksi ?>" method="post" enctype="multipart/form-data">
                                <table class="table table-bordered table-striped">
                                    <tr>
                                        <td style="text-align: right;vertical-align: middle;width: 200px"><strong>Email</strong></td>
                                        <td style="text-align: center;vertical-align: middle;width: 30px"><strong>:</strong></td>
                                        <td>
                                            <input type="email" style="width: 300px" class="form-control" name="email" placeholder="Email" value="<?= $email ?>" />
                                        </td>
                                        <td style="width: 400px">
                                            <?php echo '<b>'.form_error('email').'</b>'; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;vertical-align: middle;width: 200px"><strong>Username</strong></td>
                                        <td style="text-align: center;vertical-align: middle;width: 30px"><strong>:</strong></td>
                                        <td>
                                            <input type="text" style="width: 300px" class="form-control" name="name" placeholder="Username" value="<?= $name ?>" />
                                        </td>
                                        <td style="width: 400px">
                                            <?php echo '<b>'.form_error('name').'</b>'; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <input type="hidden" name="<?= $id ?>" value="<?php echo $id_value; ?>" /> 
                                            <button type="submit" class="btn btn-danger"><i class="fa fa-floppy-o"></i>
                                                <?= $tombol_simpan ?>
                                            </button> 
                                            <a href="<?php echo site_url($controller) ?>" class="btn btn-info"><i class="fa fa-sign-out"></i> <?= $tombol_kembali ?></a>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>