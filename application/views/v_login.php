<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
$this->CI = & get_instance();
$this->CI->config->load('string_text');
$this->string_text = $this->CI->config->item('text');
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo getInfoAPP('app_title')?></title>
        <link rel="icon" type="ico" href="<?php echo base_url(); ?>assets/img/logo_bpn.png">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/Ionicons/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/dist/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/plugins/iCheck/square/blue.css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition login-page" style="background-position: center;background-repeat: no-repeat;background-size: cover;background-image: url('<?= base_url(); ?>assets/img/background-image.png');">
        <div class="login-box">
            <div class="login-logo">
                <!--<a href="<?php echo base_url(); ?>/adminlte/index2.html"><b><?php echo getInfoAPP('app_title')?></b></a>-->
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <?php if(getInfoAPP('flag_offline') == 'ONLINE') { ?>
                <div align="center"> 
                    <img src="<?= base_url() ?>assets/img/logo_bpn.png" width="150px" height="150px">
                </div>
                <?php
                $status_login = $this->session->userdata('status_login');
                if (empty($status_login)) {
                    $message = "".$this->string_text['login']."";
                } else {
                    $message = $status_login;
                }
                if (empty($status_login)) {
                ?>
                <p class="login-box-msg"><?php echo $message; ?></p>
                <?php } else { ?>
                <div class="row" style="margin-top: 20px;">
                    <div class="col-xs-12">
                        <div class="callout callout-info">
                            <p><b><?php echo $message; ?></b></p>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <form action="<?= base_url(); ?>/c_auth/login" method="post">
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" name="name" placeholder="Username">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" name="pass" placeholder="Password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">

                        </div>
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-danger btn-block btn-flat"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12" style="text-align: center">
                            <?= "<br>".getInfoAPP('copyright')?>
                        </div>
                    </div>
                </form>
                <?php } else { ?>
                <div align="center"> 
                    <img src="<?= base_url(); ?>assets/img/offline.jpg" width="200px" height="200px">
                </div>
                <?php
                    echo '<br><div align="center">'.getInfoAPP('msg_offline').'</div><br>';
                    $status_login = $this->session->userdata('status_login');
                    if (empty($status_login)) {
                        $message = "".$this->string_text['login']."";
                    } else {
                        $message = $status_login;
                    }
                    if (empty($status_login)) {
                ?>
                <p class="login-box-msg"><?php echo $message; ?></p>
                <?php } else { ?>
                <div class="row" style="margin-top: 20px;">
                    <div class="col-xs-12">
                        <div class="callout callout-info">
                            <p><b><?php echo $message; ?></b></p>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <form action="<?= base_url(); ?>/c_auth/login" method="post">
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" name="name" placeholder="Username">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" name="pass" placeholder="Password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            
                        </div>
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-danger btn-block btn-flat"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</button>
                        </div>
                    </div>
                    <br><br>
                    <div class="row">
                        <div class="col-xs-12" style="text-align: center">
                            <?php echo getInfoAPP('copyright')?>
                        </div>
                    </div>
                </form>
                <?php } ?>
            </div>
        </div>
        
        <!-- jQuery 3 -->
        <script src="<?php echo base_url(); ?>/assets/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="<?php echo base_url(); ?>/assets/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- iCheck -->
        <script src="<?php echo base_url(); ?>/assets/adminlte/plugins/iCheck/icheck.min.js"></script>
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>
    </body>
</html>
