<?php 

$string = "<div class=\"content-wrapper\">
    <section class=\"content\">
        <div class=\"row\">
            <div class=\"col-xs-12\">
                <div class=\"box box-warning box-solid\">
                    <div class=\"box-header\">
                        <h3 class=\"box-title\"><b><?= strtoupper(\$judul) ?></b></h3>
                    </div>
                    <div class=\"box-body\">
           <div style=\"padding-bottom: 10px;\"'>
                <?php echo anchor(site_url('".$c_url."/tambah'), '<i class=\"fa fa-wpforms\" aria-hidden=\"true\"></i> Tambah Data', 'class=\"btn btn-danger btn-sm\"'); ?>";
if ($export_excel == '1') {
    $string .= "\n\t\t<?php echo anchor(site_url('".$c_url."/excel'), 'Excel', 'class=\"btn btn-primary\"'); ?>";
}
if ($export_word == '1') {
    $string .= "\n\t\t<?php echo anchor(site_url('".$c_url."/word'), 'Word', 'class=\"btn btn-primary\"'); ?>";
}
if ($export_pdf == '1') {
    $string .= "\n\t\t<?php echo anchor(site_url('".$c_url."/pdf'), 'PDF', 'class=\"btn btn-primary\"'); ?>";
}
$string .= "\n\t    </div>
        </div>
        <table class=\"table table-bordered table-striped\" id=\"mytable\">
            <thead>
                <tr>
                    <th width=\"80px\">No</th>";
foreach ($non_pk as $row) {
	if(stripos($row['column_name'],"create") !== false || stripos($row['column_name'],"update") !== false){
		
	}else{
		$string .= "\n\t\t    <th>" . label($row['column_name']) . "</th>";
	}
}
$string .= "\n\t\t    <th>Action</th>
                </tr>
            </thead>";

$string .= "</table>
        <script src=\"<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>\"></script>
        <script src=\"<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>\"></script>
        <script src=\"<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>\"></script>
        <script type=\"text/javascript\">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
        {
            return {
                \"iStart\": oSettings._iDisplayStart,
                \"iEnd\": oSettings.fnDisplayEnd(),
                \"iLength\": oSettings._iDisplayLength,
                \"iTotal\": oSettings.fnRecordsTotal(),
                \"iFilteredTotal\": oSettings.fnRecordsDisplay(),
                \"iPage\": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                \"iTotalPages\": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $(\"#mytable\").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function(e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                    }
                });
            },
            oLanguage: {
                sProcessing: \"loading...\"
            },
            processing: true,
            serverSide: true,
            ajax: {\"url\": \"$c/json\", \"type\": \"POST\"},
            columns: [{
					\"data\": \"create_date\"
					},";
			foreach ($non_pk as $row) {
				if(stripos($row['column_name'],"create") !== false || stripos($row['column_name'],"update") !== false){
					
				}else{
					$string .= "{
					\"data\": \"". $row['column_name'] ."\"
					},";
				}
			} 
            $string .= "{
                    \"data\" : \"action\",
                    \"orderable\": false,
                    \"className\" : \"text-center\"
                }";    
            $string .= "],
            order: [[0, 'asc']],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>";


$hasil_view_list = createFile($string, $target."views/" . $c_url . "/" . $v_list_file);

?>