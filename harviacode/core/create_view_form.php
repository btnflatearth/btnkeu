<?php 

$string = "
<div class=\"content-wrapper\">
    <section class=\"content\">
        <div class=\"box box-warning box-solid\">
            <div class=\"box-header with-border\">
                <h3 class=\"box-title\"><b><?= strtoupper(\$judul) ?></b></h3>
            </div>
            <form action=\"<?= \$aksi ?>\" method=\"post\" enctype=\"multipart/form-data\">
                <table class=\"table table-bordered\">";    
foreach ($non_pk as $row) {
    if(stripos($row['column_name'],"create") !== false || stripos($row['column_name'],"update") !== false){}
	else{
	$string .= "<tr>
				<td style=\"width: 200px\"><b>".$row["column_name"]."</b></td>
				<td style=\"width: 10px\"><b>:</b></td>
					<td>
                      <input type=\"text\" style=\"width: 400px\" class=\"form-control\" name=\"".$row["column_name"]."\" placeholder=\"".$row["column_name"]."\" value=\"<?= \$".$row["column_name"]."?>\" />
                            <?php echo form_error('".$row["column_name"].";') ?>
					</td>
				</tr>
				";
    }
}
$string .= "<tr>
                        <td></td>
                        <td></td>
                        <td>
                            <input type=\"hidden\" name=\"".$pk."\" value=\"<?php echo \$".$pk."; ?>\" /> 
                            <button type=\"submit\" class=\"btn btn-danger\"><i class=\"fa fa-floppy-o\"></i>
                                <?php echo \$tombol ?>
                            </button> 
                            <a href=\"<?php echo site_url('".$c_url."') ?>\" class=\"btn btn-info\"><i class=\"fa fa-sign-out\"></i> Kembali</a>
                        </td>
                    </tr>
                </table>
            </form>        
        </div>
    </section>
</div>";

$hasil_view_form = createFile($string, $target."views/" . $c_url . "/" . $v_form_file);

?>